set default_pcie_config [list \
       CONFIG.REF_CLK_FREQ {100_MHz} \
       CONFIG.PL_LINK_CAP_MAX_LINK_WIDTH {X8} \
       CONFIG.PL_LINK_CAP_MAX_LINK_SPEED {8.0_GT/s} \
       CONFIG.PF0_Use_Class_Code_Lookup_Assistant {false} \
       CONFIG.pf0_bar0_enabled {false} CONFIG.pf0_bar0_scale {Megabytes} CONFIG.pf0_bar0_size {128} \
       CONFIG.pf0_bar0_type {N/A} CONFIG.pf0_bar0_64bit {false} CONFIG.pf0_bar0_scale {Megabytes} CONFIG.pf0_bar0_size {128} \
       CONFIG.pf0_bar2_enabled {false} CONFIG.pf0_bar2_scale {Megabytes} CONFIG.pf0_bar2_size {128} \
       CONFIG.pf0_bar2_type {N/A} CONFIG.pf0_bar2_64bit {false} CONFIG.pf0_bar2_scale {Megabytes} CONFIG.pf0_bar2_size {128} \
       CONFIG.pf0_bar4_enabled {true} CONFIG.pf0_bar4_64bit {true} CONFIG.pf0_bar4_scale {Megabytes} CONFIG.pf0_bar4_size {128} \
       CONFIG.pf0_bar4_type {Memory} CONFIG.pf0_bar4_scale {Megabytes} CONFIG.pf0_bar4_size {128} \
       CONFIG.pf0_expansion_rom_enabled {false} \
       CONFIG.PF0_INTERRUPT_PIN {NONE} \
       CONFIG.pf0_msi_enabled {false} \
       CONFIG.mode_selection {Advanced} \
       CONFIG.pipe_sim {false} \
       CONFIG.axisten_freq {250} \
       CONFIG.cfg_fc_if {false} \
       CONFIG.cfg_status_if {true} \
       CONFIG.per_func_status_if {false} \
       CONFIG.cfg_mgmt_if {false} \
       CONFIG.rcv_msg_if {false} \
       CONFIG.cfg_tx_msg_if {false} \
       CONFIG.cfg_ctl_if {false} \
       CONFIG.tx_fc_if {false} \
       CONFIG.en_transceiver_status_ports {false} \
       CONFIG.PL_LINK_CAP_MAX_LINK_WIDTH {X8} \
       CONFIG.PL_LINK_CAP_MAX_LINK_SPEED {8.0_GT/s} \
       CONFIG.axisten_if_width {256_bit} \
       CONFIG.AXISTEN_IF_RC_STRADDLE {true} \
       CONFIG.PF0_DEVICE_ID {8038} \
       CONFIG.PF1_DEVICE_ID {8011} \
       CONFIG.pf0_expansion_rom_size {2} \
       CONFIG.PF0_MSIX_CAP_TABLE_BIR {BAR_5:4} \
       CONFIG.PF0_MSIX_CAP_PBA_BIR {BAR_5:4} \
       CONFIG.PF1_MSIX_CAP_TABLE_BIR {BAR_0} \
       CONFIG.PF1_MSIX_CAP_PBA_BIR {BAR_0} \
       CONFIG.axisten_freq {250} \
       CONFIG.aspm_support {No_ASPM} \
       CONFIG.dedicate_perst {false} \
       CONFIG.mcap_enablement {None} \
       CONFIG.coreclk_freq {500} \
       CONFIG.plltype {QPLL1} \
       CONFIG.pf0_bar0_enabled {true} CONFIG.pf0_bar0_64bit {true} CONFIG.pf0_bar0_scale {Megabytes} CONFIG.pf0_bar0_size {128} \
       CONFIG.pf0_bar4_enabled {false} CONFIG.pf0_bar0_type {Memory} CONFIG.pf0_bar0_scale {Megabytes} CONFIG.pf0_bar0_size {128} \
       CONFIG.pf0_bar4_type {N/A} CONFIG.pf0_bar4_64bit {false} CONFIG.pf0_bar4_scale {Megabytes} CONFIG.pf0_bar4_size {128} \
       CONFIG.PF0_MSIX_CAP_TABLE_BIR {BAR_1:0} \
       CONFIG.PF0_MSIX_CAP_PBA_BIR {BAR_1:0} \
       CONFIG.PF1_MSIX_CAP_TABLE_BIR {BAR_0} \
       CONFIG.PF1_MSIX_CAP_PBA_BIR {BAR_0} \
       CONFIG.pf0_base_class_menu {Data_acquisition_and_signal_processing_controllers} \
       CONFIG.pf0_sub_class_interface_menu {Other_data_acquisition/signal_processing_controllers} \
       CONFIG.pf0_class_code_base {11} \
       CONFIG.pf0_class_code_sub {80} \
       CONFIG.PF0_CLASS_CODE {118000} \
   ] \

create_ip -name pcie3_ultrascale -vendor xilinx.com -library ip -version 4.4 -module_name pcie3_ultrascale_X0Y3
set_property -dict ${default_pcie_config} [get_ips pcie3_ultrascale_X0Y3]
set_property -dict [list \
        CONFIG.pcie_blk_locn {X0Y3} \
        CONFIG.gen_x0y0 {false} \
        CONFIG.gen_x0y1 {false} \
        CONFIG.gen_x0y3 {true} \
        CONFIG.select_quad {GTH_Quad_230} \
   ] \
   [get_ips pcie3_ultrascale_X0Y3]

create_ip -name pcie3_ultrascale -vendor xilinx.com -library ip -version 4.4 -module_name pcie3_ultrascale_X0Y1
set_property -dict ${default_pcie_config} [get_ips pcie3_ultrascale_X0Y1]
set_property -dict [list \
        CONFIG.pcie_blk_locn {X0Y1} \
        CONFIG.gen_x0y0 {false} \
        CONFIG.gen_x0y1 {true} \
        CONFIG.gen_x0y3 {false} \
        CONFIG.select_quad {GTH_Quad_227} \
    ] \
    [get_ips pcie3_ultrascale_X0Y1]

