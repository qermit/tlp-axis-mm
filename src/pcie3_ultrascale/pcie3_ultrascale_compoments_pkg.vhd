----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/07/2020 02:11:34 AM
-- Design Name: 
-- Module Name: pcie3_ultrascale_compoments_pkg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;

use work.pcie_gen3_pkg.all;

package pcie3_ultrascale_compoments_pkg is

component pcie3_ultrascale_ep is
  generic(
    g_pcie_block: string:= "X0Y1"
  );
  Port (
    pci_exp_txn : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    pci_exp_txp : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    pci_exp_rxn : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    pci_exp_rxp : IN STD_LOGIC_VECTOR(7 DOWNTO 0);

    sys_clk : IN STD_LOGIC;
    sys_clk_gt : IN STD_LOGIC;
    sys_reset : IN STD_LOGIC;
    
    user_clk : OUT STD_LOGIC;
    user_reset : OUT STD_LOGIC;
    user_lnk_up : OUT STD_LOGIC;
    
    s_axis_rq_tdata : IN STD_LOGIC_VECTOR(255 DOWNTO 0) := (others => '0');
    s_axis_rq_tkeep : IN STD_LOGIC_VECTOR(7 DOWNTO 0) := (others => '0');
    s_axis_rq_tlast : IN STD_LOGIC := '0';
    s_axis_rq_tready : OUT STD_LOGIC_VECTOR(3 DOWNTO 0) ;
    s_axis_rq_tuser : IN STD_LOGIC_VECTOR(59 DOWNTO 0) := (others => '0');
    s_axis_rq_tvalid : IN STD_LOGIC := '0';
    m_axis_rc_tdata : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    m_axis_rc_tkeep : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axis_rc_tlast : OUT STD_LOGIC;
    m_axis_rc_tready : IN STD_LOGIC := '0';
    m_axis_rc_tuser : OUT STD_LOGIC_VECTOR(74 DOWNTO 0);
    m_axis_rc_tvalid : OUT STD_LOGIC;
    
    m_axis_cq_m2s: out t_axis_cq_m2s;
    m_axis_cq_s2m: in  t_axis_cq_s2m;           
    s_axis_cc_m2s: in  t_axis_cc_m2s;
    s_axis_cc_s2m: out t_axis_cc_s2m;
    
    cfg_interrupt_int : IN STD_LOGIC_VECTOR(3 DOWNTO 0) := (others => '0');
    cfg_interrupt_pending : IN STD_LOGIC_VECTOR(3 DOWNTO 0) := (others => '0');
    cfg_interrupt_sent : OUT STD_LOGIC;
    -- debug ports
    cfg_ltssm_state: out std_logic_vector(5 downto 0)
  );
end component pcie3_ultrascale_ep;

COMPONENT vio_pcie_test
  PORT (
    clk : IN STD_LOGIC;
    probe_in0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    probe_in1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0)
  );
END COMPONENT;
-- COMP_TAG_END ------ End COMPONENT Declaration ------------

-- The following code must appear in the VHDL architecture
-- body. Substitute your own instance name and net names.

------------- Begin Cut here for INSTANTIATION Template ----- INST_TAG

end package pcie3_ultrascale_compoments_pkg;

package body pcie3_ultrascale_compoments_pkg is
  
end package body pcie3_ultrascale_compoments_pkg;
