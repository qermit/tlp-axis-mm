----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/07/2020 01:53:27 AM
-- Design Name: 
-- Module Name: pcie3_ultrascale_ep - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.pcie_gen3_pkg.all;


entity pcie3_ultrascale_ep is
  generic(
    g_pcie_block: string:= "X0Y1"
  );
  Port (
    pci_exp_txn : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    pci_exp_txp : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    pci_exp_rxn : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    pci_exp_rxp : IN STD_LOGIC_VECTOR(7 DOWNTO 0);

    sys_clk : IN STD_LOGIC;
    sys_clk_gt : IN STD_LOGIC;
    sys_reset : IN STD_LOGIC;

    user_clk : OUT STD_LOGIC;
    user_reset : OUT STD_LOGIC;
    user_lnk_up : OUT STD_LOGIC;

    s_axis_rq_tdata : IN STD_LOGIC_VECTOR(255 DOWNTO 0) := (others => '0');
    s_axis_rq_tkeep : IN STD_LOGIC_VECTOR(7 DOWNTO 0) := (others => '0');
    s_axis_rq_tlast : IN STD_LOGIC := '0';
    s_axis_rq_tready : OUT STD_LOGIC_VECTOR(3 DOWNTO 0) ;
    s_axis_rq_tuser : IN STD_LOGIC_VECTOR(59 DOWNTO 0) := (others => '0');
    s_axis_rq_tvalid : IN STD_LOGIC := '0';
    m_axis_rc_tdata : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    m_axis_rc_tkeep : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axis_rc_tlast : OUT STD_LOGIC;
    m_axis_rc_tready : IN STD_LOGIC := '0';
    m_axis_rc_tuser : OUT STD_LOGIC_VECTOR(74 DOWNTO 0);
    m_axis_rc_tvalid : OUT STD_LOGIC;

    m_axis_cq_m2s: out t_axis_cq_m2s;
    m_axis_cq_s2m: in  t_axis_cq_s2m;
    s_axis_cc_m2s: in  t_axis_cc_m2s;
    s_axis_cc_s2m: out t_axis_cc_s2m;

    cfg_interrupt_int : IN STD_LOGIC_VECTOR(3 DOWNTO 0) := (others => '0');
    cfg_interrupt_pending : IN STD_LOGIC_VECTOR(3 DOWNTO 0) := (others => '0');
    cfg_interrupt_sent : OUT STD_LOGIC;
    -- debug ports
    cfg_ltssm_state: out std_logic_vector(5 downto 0)
  );
end pcie3_ultrascale_ep;

architecture Behavioral of pcie3_ultrascale_ep is

COMPONENT pcie3_ultrascale_X0Y1
  PORT (
    pci_exp_txn : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    pci_exp_txp : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    pci_exp_rxn : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    pci_exp_rxp : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    user_clk : OUT STD_LOGIC;
    user_reset : OUT STD_LOGIC;
    user_lnk_up : OUT STD_LOGIC;
    s_axis_rq_tdata : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    s_axis_rq_tkeep : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axis_rq_tlast : IN STD_LOGIC;
    s_axis_rq_tready : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axis_rq_tuser : IN STD_LOGIC_VECTOR(59 DOWNTO 0);
    s_axis_rq_tvalid : IN STD_LOGIC;
    m_axis_rc_tdata : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    m_axis_rc_tkeep : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axis_rc_tlast : OUT STD_LOGIC;
    m_axis_rc_tready : IN STD_LOGIC;
    m_axis_rc_tuser : OUT STD_LOGIC_VECTOR(74 DOWNTO 0);
    m_axis_rc_tvalid : OUT STD_LOGIC;

    m_axis_cq_tdata : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    m_axis_cq_tkeep : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axis_cq_tlast : OUT STD_LOGIC;
    m_axis_cq_tready : IN STD_LOGIC;
    m_axis_cq_tuser : OUT STD_LOGIC_VECTOR(84 DOWNTO 0);
    m_axis_cq_tvalid : OUT STD_LOGIC;
    s_axis_cc_tdata : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    s_axis_cc_tkeep : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axis_cc_tlast : IN STD_LOGIC;
    s_axis_cc_tready : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axis_cc_tuser : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    s_axis_cc_tvalid : IN STD_LOGIC;

    pcie_rq_seq_num : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    pcie_rq_seq_num_vld : OUT STD_LOGIC;
    pcie_rq_tag : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    pcie_rq_tag_av : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    pcie_rq_tag_vld : OUT STD_LOGIC;
    pcie_cq_np_req : IN STD_LOGIC;
    pcie_cq_np_req_count : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    cfg_phy_link_down : OUT STD_LOGIC;
    cfg_phy_link_status : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    cfg_negotiated_width : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_current_speed : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    cfg_max_payload : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    cfg_max_read_req : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    cfg_function_status : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    cfg_function_power_state : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    cfg_vf_status : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    cfg_vf_power_state : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
    cfg_link_power_state : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    cfg_err_cor_out : OUT STD_LOGIC;
    cfg_err_nonfatal_out : OUT STD_LOGIC;
    cfg_err_fatal_out : OUT STD_LOGIC;
    cfg_ltr_enable : OUT STD_LOGIC;
    cfg_ltssm_state : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    cfg_rcb_status : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_dpa_substate_change : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_obff_enable : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    cfg_pl_status_change : OUT STD_LOGIC;
    cfg_tph_requester_enable : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_tph_st_mode : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    cfg_vf_tph_requester_enable : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_vf_tph_st_mode : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
    cfg_interrupt_int : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_interrupt_pending : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_interrupt_sent : OUT STD_LOGIC;
    sys_clk : IN STD_LOGIC;
    sys_clk_gt : IN STD_LOGIC;
    sys_reset : IN STD_LOGIC;
    int_qpll1lock_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    int_qpll1outrefclk_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    int_qpll1outclk_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    phy_rdy_out : OUT STD_LOGIC
  );
END COMPONENT;


COMPONENT pcie3_ultrascale_X0Y3
  PORT (
    pci_exp_txn : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    pci_exp_txp : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    pci_exp_rxn : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    pci_exp_rxp : IN STD_LOGIC_VECTOR(7 DOWNTO 0);

    user_clk : OUT STD_LOGIC;
    user_reset : OUT STD_LOGIC;
    user_lnk_up : OUT STD_LOGIC;

    s_axis_rq_tdata : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    s_axis_rq_tkeep : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axis_rq_tlast : IN STD_LOGIC;
    s_axis_rq_tready : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axis_rq_tuser : IN STD_LOGIC_VECTOR(59 DOWNTO 0);
    s_axis_rq_tvalid : IN STD_LOGIC;
    m_axis_rc_tdata : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    m_axis_rc_tkeep : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axis_rc_tlast : OUT STD_LOGIC;
    m_axis_rc_tready : IN STD_LOGIC;
    m_axis_rc_tuser : OUT STD_LOGIC_VECTOR(74 DOWNTO 0);
    m_axis_rc_tvalid : OUT STD_LOGIC;
    m_axis_cq_tdata : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    m_axis_cq_tkeep : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axis_cq_tlast : OUT STD_LOGIC;
    m_axis_cq_tready : IN STD_LOGIC;
    m_axis_cq_tuser : OUT STD_LOGIC_VECTOR(84 DOWNTO 0);
    m_axis_cq_tvalid : OUT STD_LOGIC;
    s_axis_cc_tdata : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    s_axis_cc_tkeep : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axis_cc_tlast : IN STD_LOGIC;
    s_axis_cc_tready : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axis_cc_tuser : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    s_axis_cc_tvalid : IN STD_LOGIC;

    pcie_rq_seq_num : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    pcie_rq_seq_num_vld : OUT STD_LOGIC;
    pcie_rq_tag : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    pcie_rq_tag_av : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    pcie_rq_tag_vld : OUT STD_LOGIC;
    pcie_cq_np_req : IN STD_LOGIC;
    pcie_cq_np_req_count : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);

    cfg_phy_link_down : OUT STD_LOGIC;
    cfg_phy_link_status : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    cfg_negotiated_width : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_current_speed : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    cfg_max_payload : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    cfg_max_read_req : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    cfg_function_status : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    cfg_function_power_state : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);

    cfg_vf_status : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    cfg_vf_power_state : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
    cfg_link_power_state : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    cfg_err_cor_out : OUT STD_LOGIC;
    cfg_err_nonfatal_out : OUT STD_LOGIC;
    cfg_err_fatal_out : OUT STD_LOGIC;
    cfg_ltr_enable : OUT STD_LOGIC;
    cfg_ltssm_state : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);

    cfg_rcb_status : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_dpa_substate_change : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_obff_enable : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    cfg_pl_status_change : OUT STD_LOGIC;
    cfg_tph_requester_enable : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_tph_st_mode : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    cfg_vf_tph_requester_enable : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_vf_tph_st_mode : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);

    cfg_interrupt_int : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_interrupt_pending : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_interrupt_sent : OUT STD_LOGIC;

    sys_clk : IN STD_LOGIC;
    sys_clk_gt : IN STD_LOGIC;
    sys_reset : IN STD_LOGIC;

    int_qpll1lock_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    int_qpll1outrefclk_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    int_qpll1outclk_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);

    phy_rdy_out : OUT STD_LOGIC
  );
END COMPONENT;

COMPONENT ila_pcie_cfg_status

PORT (
    clk : IN STD_LOGIC;



    probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe1 : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    probe2 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    probe3 : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    probe4 : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    probe5 : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    probe6 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe7 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    probe8 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe9 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
    probe10 : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    probe11 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe12 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe13 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe14 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe15 : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    probe16 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    probe17 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    probe18 : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    probe19 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe20 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    probe21 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    probe22 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    probe23 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
    probe24 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe25 : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    probe26 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
);
END COMPONENT  ;


signal m_axis_cq_tdata :  STD_LOGIC_VECTOR(255 DOWNTO 0);
signal m_axis_cq_tkeep :  STD_LOGIC_VECTOR(7 DOWNTO 0);
signal m_axis_cq_tlast :  STD_LOGIC;
signal m_axis_cq_tready :  STD_LOGIC;
signal m_axis_cq_tuser :  STD_LOGIC_VECTOR(84 DOWNTO 0);
signal m_axis_cq_tvalid :  STD_LOGIC;
signal s_axis_cc_tdata :  STD_LOGIC_VECTOR(255 DOWNTO 0);
signal s_axis_cc_tkeep :  STD_LOGIC_VECTOR(7 DOWNTO 0);
signal s_axis_cc_tlast :  STD_LOGIC;
signal s_axis_cc_tready :  STD_LOGIC_VECTOR(3 DOWNTO 0);
signal s_axis_cc_tuser :  STD_LOGIC_VECTOR(32 DOWNTO 0);
signal s_axis_cc_tvalid :  STD_LOGIC;

signal pcie_cq_np_req :  STD_LOGIC := '1';
signal pcie_cq_np_req_count :  STD_LOGIC_VECTOR(5 DOWNTO 0);
signal pcie_rq_seq_num :  STD_LOGIC_VECTOR(3 DOWNTO 0);
signal pcie_rq_seq_num_vld :  STD_LOGIC;
signal pcie_rq_tag :  STD_LOGIC_VECTOR(5 DOWNTO 0);
signal pcie_rq_tag_av :  STD_LOGIC_VECTOR(1 DOWNTO 0);
signal pcie_rq_tag_vld :  STD_LOGIC;

signal    cfg_phy_link_down :  STD_LOGIC;
signal    cfg_phy_link_status :  STD_LOGIC_VECTOR(1 DOWNTO 0);
signal    cfg_negotiated_width :  STD_LOGIC_VECTOR(3 DOWNTO 0);
signal    cfg_current_speed :  STD_LOGIC_VECTOR(2 DOWNTO 0);
signal    cfg_max_payload :  STD_LOGIC_VECTOR(2 DOWNTO 0);
signal    cfg_max_read_req :  STD_LOGIC_VECTOR(2 DOWNTO 0);
signal    cfg_function_status :  STD_LOGIC_VECTOR(15 DOWNTO 0);
signal    cfg_function_power_state :  STD_LOGIC_VECTOR(11 DOWNTO 0);
signal    cfg_vf_status :  STD_LOGIC_VECTOR(15 DOWNTO 0);
signal    cfg_vf_power_state :  STD_LOGIC_VECTOR(23 DOWNTO 0);
signal    cfg_link_power_state :  STD_LOGIC_VECTOR(1 DOWNTO 0);
signal    cfg_err_cor_out :  STD_LOGIC;
signal    cfg_err_nonfatal_out :  STD_LOGIC;
signal    cfg_err_fatal_out :  STD_LOGIC;
signal    cfg_ltr_enable :  STD_LOGIC;
signal    cfg_rcb_status :  STD_LOGIC_VECTOR(3 DOWNTO 0);
signal    cfg_dpa_substate_change :  STD_LOGIC_VECTOR(3 DOWNTO 0);
signal    cfg_obff_enable :  STD_LOGIC_VECTOR(1 DOWNTO 0);
signal    cfg_pl_status_change :  STD_LOGIC;
signal    cfg_tph_requester_enable :  STD_LOGIC_VECTOR(3 DOWNTO 0);
signal    cfg_tph_st_mode :  STD_LOGIC_VECTOR(11 DOWNTO 0);
signal    cfg_vf_tph_requester_enable :  STD_LOGIC_VECTOR(7 DOWNTO 0);
signal    cfg_vf_tph_st_mode :  STD_LOGIC_VECTOR(23 DOWNTO 0);

signal    int_qpll1lock_out :  STD_LOGIC_VECTOR(1 DOWNTO 0);
signal    int_qpll1outrefclk_out :  STD_LOGIC_VECTOR(1 DOWNTO 0);
signal    int_qpll1outclk_out :  STD_LOGIC_VECTOR(1 DOWNTO 0);
signal    phy_rdy_out :  STD_LOGIC;


signal s_user_clk: std_logic;
signal s_cfg_ltssm_state: std_logic_vector(cfg_ltssm_state'range); 
begin

m_axis_cq_m2s.tdata  <= m_axis_cq_tdata ;
m_axis_cq_m2s.tkeep  <= m_axis_cq_tkeep ;
m_axis_cq_m2s.tlast  <= m_axis_cq_tlast ;
m_axis_cq_m2s.tuser  <= m_axis_cq_tuser ;
m_axis_cq_m2s.tvalid <= m_axis_cq_tvalid ;
m_axis_cq_tready     <= m_axis_cq_s2m.tready;

s_axis_cc_tdata  <= s_axis_cc_m2s.tdata ;
s_axis_cc_tkeep  <= s_axis_cc_m2s.tkeep ;
s_axis_cc_tlast  <= s_axis_cc_m2s.tlast ;
s_axis_cc_tuser  <= s_axis_cc_m2s.tuser ;
s_axis_cc_tvalid <= s_axis_cc_m2s.tvalid ;   
s_axis_cc_s2m.tready <= s_axis_cc_tready(0);

user_clk <= s_user_clk;
cfg_ltssm_state <= s_cfg_ltssm_state;

GEN_X0Y1:
  if  g_pcie_block = "X0Y1" generate
    u_pcie : pcie3_ultrascale_X0Y1
  PORT MAP (
    pci_exp_txn => pci_exp_txn,
    pci_exp_txp => pci_exp_txp,
    pci_exp_rxn => pci_exp_rxn,
    pci_exp_rxp => pci_exp_rxp,
    user_clk => s_user_clk,
    user_reset => user_reset,
    user_lnk_up => user_lnk_up,
    s_axis_rq_tdata => s_axis_rq_tdata,
    s_axis_rq_tkeep => s_axis_rq_tkeep,
    s_axis_rq_tlast => s_axis_rq_tlast,
    s_axis_rq_tready => s_axis_rq_tready,
    s_axis_rq_tuser => s_axis_rq_tuser,
    s_axis_rq_tvalid => s_axis_rq_tvalid,
    m_axis_rc_tdata => m_axis_rc_tdata,
    m_axis_rc_tkeep => m_axis_rc_tkeep,
    m_axis_rc_tlast => m_axis_rc_tlast,
    m_axis_rc_tready => m_axis_rc_tready,
    m_axis_rc_tuser => m_axis_rc_tuser,
    m_axis_rc_tvalid => m_axis_rc_tvalid,
    m_axis_cq_tdata => m_axis_cq_tdata,
    m_axis_cq_tkeep => m_axis_cq_tkeep,
    m_axis_cq_tlast => m_axis_cq_tlast,
    m_axis_cq_tready => m_axis_cq_tready,
    m_axis_cq_tuser => m_axis_cq_tuser,
    m_axis_cq_tvalid => m_axis_cq_tvalid,
    s_axis_cc_tdata => s_axis_cc_tdata,
    s_axis_cc_tkeep => s_axis_cc_tkeep,
    s_axis_cc_tlast => s_axis_cc_tlast,
    s_axis_cc_tready => s_axis_cc_tready,
    s_axis_cc_tuser => s_axis_cc_tuser,
    s_axis_cc_tvalid => s_axis_cc_tvalid,
    pcie_rq_seq_num => pcie_rq_seq_num,
    pcie_rq_seq_num_vld => pcie_rq_seq_num_vld,
    pcie_rq_tag => pcie_rq_tag,
    pcie_rq_tag_av => pcie_rq_tag_av,
    pcie_rq_tag_vld => pcie_rq_tag_vld,
    pcie_cq_np_req => pcie_cq_np_req,
    pcie_cq_np_req_count => pcie_cq_np_req_count,
    cfg_phy_link_down => cfg_phy_link_down,
    cfg_phy_link_status => cfg_phy_link_status,
    cfg_negotiated_width => cfg_negotiated_width,
    cfg_current_speed => cfg_current_speed,
    cfg_max_payload => cfg_max_payload,
    cfg_max_read_req => cfg_max_read_req,
    cfg_function_status => cfg_function_status,
    cfg_function_power_state => cfg_function_power_state,
    cfg_vf_status => cfg_vf_status,
    cfg_vf_power_state => cfg_vf_power_state,
    cfg_link_power_state => cfg_link_power_state,
    cfg_err_cor_out => cfg_err_cor_out,
    cfg_err_nonfatal_out => cfg_err_nonfatal_out,
    cfg_err_fatal_out => cfg_err_fatal_out,
    cfg_ltr_enable => cfg_ltr_enable,
    cfg_ltssm_state => s_cfg_ltssm_state,
    cfg_rcb_status => cfg_rcb_status,
    cfg_dpa_substate_change => cfg_dpa_substate_change,
    cfg_obff_enable => cfg_obff_enable,
    cfg_pl_status_change => cfg_pl_status_change,
    cfg_tph_requester_enable => cfg_tph_requester_enable,
    cfg_tph_st_mode => cfg_tph_st_mode,
    cfg_vf_tph_requester_enable => cfg_vf_tph_requester_enable,
    cfg_vf_tph_st_mode => cfg_vf_tph_st_mode,

    cfg_interrupt_int => cfg_interrupt_int,
    cfg_interrupt_pending => cfg_interrupt_pending,
    cfg_interrupt_sent => cfg_interrupt_sent,
    sys_clk => sys_clk,
    sys_clk_gt => sys_clk_gt,
    sys_reset => sys_reset,

    int_qpll1lock_out => int_qpll1lock_out,
    int_qpll1outrefclk_out => int_qpll1outrefclk_out,
    int_qpll1outclk_out => int_qpll1outclk_out,
    phy_rdy_out => phy_rdy_out
  );
  end generate;

GEN_X0Y3: if  g_pcie_block = "X0Y3" generate
begin
  u_pcie : pcie3_ultrascale_X0Y3
  PORT MAP (
    pci_exp_txn => pci_exp_txn,
    pci_exp_txp => pci_exp_txp,
    pci_exp_rxn => pci_exp_rxn,
    pci_exp_rxp => pci_exp_rxp,
    user_clk => s_user_clk,
    user_reset => user_reset,
    user_lnk_up => user_lnk_up,
    s_axis_rq_tdata => s_axis_rq_tdata,
    s_axis_rq_tkeep => s_axis_rq_tkeep,
    s_axis_rq_tlast => s_axis_rq_tlast,
    s_axis_rq_tready => s_axis_rq_tready,
    s_axis_rq_tuser => s_axis_rq_tuser,
    s_axis_rq_tvalid => s_axis_rq_tvalid,
    m_axis_rc_tdata => m_axis_rc_tdata,
    m_axis_rc_tkeep => m_axis_rc_tkeep,
    m_axis_rc_tlast => m_axis_rc_tlast,
    m_axis_rc_tready => m_axis_rc_tready,
    m_axis_rc_tuser => m_axis_rc_tuser,
    m_axis_rc_tvalid => m_axis_rc_tvalid,
    m_axis_cq_tdata => m_axis_cq_tdata,
    m_axis_cq_tkeep => m_axis_cq_tkeep,
    m_axis_cq_tlast => m_axis_cq_tlast,
    m_axis_cq_tready => m_axis_cq_tready,
    m_axis_cq_tuser => m_axis_cq_tuser,
    m_axis_cq_tvalid => m_axis_cq_tvalid,
    s_axis_cc_tdata => s_axis_cc_tdata,
    s_axis_cc_tkeep => s_axis_cc_tkeep,
    s_axis_cc_tlast => s_axis_cc_tlast,
    s_axis_cc_tready => s_axis_cc_tready,
    s_axis_cc_tuser => s_axis_cc_tuser,
    s_axis_cc_tvalid => s_axis_cc_tvalid,
    pcie_rq_seq_num => pcie_rq_seq_num,
    pcie_rq_seq_num_vld => pcie_rq_seq_num_vld,
    pcie_rq_tag => pcie_rq_tag,
    pcie_rq_tag_av => pcie_rq_tag_av,
    pcie_rq_tag_vld => pcie_rq_tag_vld,
    pcie_cq_np_req => pcie_cq_np_req,
    pcie_cq_np_req_count => pcie_cq_np_req_count,
    cfg_phy_link_down => cfg_phy_link_down,
    cfg_phy_link_status => cfg_phy_link_status,
    cfg_negotiated_width => cfg_negotiated_width,
    cfg_current_speed => cfg_current_speed,
    cfg_max_payload => cfg_max_payload,
    cfg_max_read_req => cfg_max_read_req,
    cfg_function_status => cfg_function_status,
    cfg_function_power_state => cfg_function_power_state,
    cfg_vf_status => cfg_vf_status,
    cfg_vf_power_state => cfg_vf_power_state,
    cfg_link_power_state => cfg_link_power_state,
    cfg_err_cor_out => cfg_err_cor_out,
    cfg_err_nonfatal_out => cfg_err_nonfatal_out,
    cfg_err_fatal_out => cfg_err_fatal_out,
    cfg_ltr_enable => cfg_ltr_enable,
    cfg_ltssm_state => s_cfg_ltssm_state,
    cfg_rcb_status => cfg_rcb_status,
    cfg_dpa_substate_change => cfg_dpa_substate_change,
    cfg_obff_enable => cfg_obff_enable,
    cfg_pl_status_change => cfg_pl_status_change,
    cfg_tph_requester_enable => cfg_tph_requester_enable,
    cfg_tph_st_mode => cfg_tph_st_mode,
    cfg_vf_tph_requester_enable => cfg_vf_tph_requester_enable,
    cfg_vf_tph_st_mode => cfg_vf_tph_st_mode,
    cfg_interrupt_int => cfg_interrupt_int,
    cfg_interrupt_pending => cfg_interrupt_pending,
    cfg_interrupt_sent => cfg_interrupt_sent,
    sys_clk => sys_clk,
    sys_clk_gt => sys_clk_gt,
    sys_reset => sys_reset,
    int_qpll1lock_out => int_qpll1lock_out,
    int_qpll1outrefclk_out => int_qpll1outrefclk_out,
    int_qpll1outclk_out => int_qpll1outclk_out,
    phy_rdy_out => phy_rdy_out
  );

end generate;

--u_ila_pcie_cfg_status : ila_pcie_cfg_status
--PORT MAP (
--    clk => s_user_clk,
--    probe0 => ( 0 => cfg_phy_link_down ),
--    probe1 => cfg_phy_link_status,
--    probe2 => cfg_negotiated_width,
--    probe3 => cfg_current_speed,
--    probe4 => cfg_max_payload,
--    probe5 => cfg_max_read_req,
--    probe6 => cfg_function_status,
--    probe7 => cfg_function_power_state,

--    probe8 => cfg_vf_status,
--    probe9 => cfg_vf_power_state,
--    probe10 => cfg_link_power_state,
--    probe11 => ( 0 => cfg_err_cor_out ),
--    probe12 => ( 0 => cfg_err_nonfatal_out ),
--    probe13 => ( 0 => cfg_err_fatal_out ),
--    probe14 => ( 0 => cfg_ltr_enable ),
--    probe15 => s_cfg_ltssm_state,

--    probe16 => cfg_rcb_status,
--    probe17 => cfg_dpa_substate_change,
--    probe18 => cfg_obff_enable,
--    probe19 => ( 0 => cfg_pl_status_change ),
--    probe20 => cfg_tph_requester_enable,
--    probe21 => cfg_tph_st_mode,
--    probe22 => cfg_vf_tph_requester_enable,
--    probe23 => cfg_vf_tph_st_mode,
--    probe24 => ( 0 => sys_reset),
--    probe25 => int_qpll1lock_out,
--    probe26 => ( 0 => phy_rdy_out)
--);

end Behavioral;
