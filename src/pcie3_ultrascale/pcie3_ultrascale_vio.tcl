create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_pcie_test
set_property -dict [list CONFIG.C_PROBE_IN0_WIDTH {8} CONFIG.C_PROBE_IN1_WIDTH {8} CONFIG.C_NUM_PROBE_OUT {0} CONFIG.C_NUM_PROBE_IN {2}] [get_ips vio_pcie_test]
