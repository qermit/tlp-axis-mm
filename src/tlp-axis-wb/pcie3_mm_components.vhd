-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : pcie3_mm_components.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.pcie3_mm_pkg.all;
use work.pcie_gen3_pkg.all;

package pcie3_mm_components_pkg is

component pcie3_tlp_to_wb is
    generic(
       wb_data_width: natural := 64;
       g_asynchronous: boolean:= false       
    );
    Port ( clk_i   : in STD_LOGIC;
           rst_n_i : in STD_LOGIC;
           
           clk2_i : in std_logic;
           rst2_n_i: in std_logic;
           
           -- slave completer request interface
           s_axis_cq_m2s: in  t_axis_cq_m2s;
           s_axis_cq_s2m: out t_axis_cq_s2m;
           -- master completer completion interface           
           m_axis_cc_m2s: out t_axis_cc_m2s;
           m_axis_cc_s2m: in  t_axis_cc_s2m;
           
           -- Wishbone master interface
           m_wb_m2s: out t_wishbone_a32_d32_m2s;
           m_wb_s2m: in  t_wishbone_a32_d32_s2m
                        
           );
end component pcie3_tlp_to_wb;



 component pcie3_tlp_pending_fifo is
  generic (
    g_tdata_width: natural:= 128
  );
  port (
    clk_i: in std_logic;
    rst_n_i: in std_logic;
    -- cq header
    s_m2s_tvalid: in std_logic;
    s_m2s_tdata:  in std_logic_vector(g_tdata_width-1 downto 0);
    s_s2m_tready: out std_logic;
    
    m_m2s_tvalid: out std_logic;
    m_m2s_tdata:  out std_logic_vector(g_tdata_width-1 downto 0);
    m_s2m_tready: in std_logic
   );
  end component pcie3_tlp_pending_fifo;

 component pcie3_tlp_fifo is
  generic (
    g_tdata_width: natural  range 8 to 2048:= 128;
    g_depth: natural range 16 to 4194304:= 16;
    g_async: boolean:=false
  );
  port (
    clk_i: in std_logic;
    rst_n_i: in std_logic;

    clk2_i: in std_logic;
    rst2_n_i: in std_logic;
    -- cq header
    s_m2s_tvalid: in std_logic;
    s_m2s_tdata:  in std_logic_vector(g_tdata_width-1 downto 0);
    s_s2m_tready: out std_logic;
    
    m_m2s_tvalid: out std_logic;
    m_m2s_tdata:  out std_logic_vector(g_tdata_width-1 downto 0);
    m_s2m_tready: in std_logic
   );
  end component pcie3_tlp_fifo;
  
  component pcie3_tlp_cq_to_wb is
  generic (
    g_tdata_width: natural:= 128
  );
  Port ( clk_i : in STD_LOGIC;
         rst_n_i : in STD_LOGIC;
           
         s_axis_cq_m2s: in  t_axis_cq_m2s;
         s_axis_cq_s2m: out t_axis_cq_s2m; 
         
         m_wb_m2s: out t_wishbone_a32_d32_m2s;
         m_wb_m2s_tuser: out std_logic_vector(2 downto 0);
         m_wb_s2m_tready: in std_logic;
                  
         m_fifo_m2s_tvalid: out std_logic;
         m_fifo_m2s_tdata:  out t_pending_packet;
         m_fifo_s2m_tready: in std_logic
  );
end component pcie3_tlp_cq_to_wb;

component pcie3_tlp_wb_to_cc is
  generic (
     g_tdata_width: natural := 128
  );
  Port ( clk_i : in STD_LOGIC;
         rst_n_i : in STD_LOGIC;
           
         s_axis_cc_m2s: out t_axis_cc_m2s;
         s_axis_cc_s2m: in  t_axis_cc_s2m;
          
         -- wishbone return fifo
         s_wb_s2m: in t_wishbone_a32_d32_s2m;
         s_wb_s2m_tuser: in std_logic_vector(2 downto 0);
         s_wb_s2m_tvalid: in  std_logic;
         s_wb_m2s_tready: out std_logic;

         -- pending requests fifo
         s_fifo_m2s_tvalid: in std_logic;
         s_fifo_m2s_tdata:  in t_pending_packet;
         s_fifo_s2m_tready: out std_logic
   );
end component pcie3_tlp_wb_to_cc;

-- wishbonne return path to tlp-axis
component pcie3_wb_to_tlp_fifo is
    generic(
       g_asynchronous: boolean:= false       
    );
    Port ( clk_i : in STD_LOGIC;
         rst_n_i : in STD_LOGIC;
         
         clk_wb_i: in std_logic;
         rst_wb_n_i: in std_logic;
         
         -- output to cc
         m_cc_wb_s2m: out t_wishbone_a32_d32_s2m;
         m_cc_wb_s2m_tuser: out std_logic_vector(2 downto 0);
         m_cc_wb_s2m_tvalid: out std_logic;
         m_cc_wb_s2m_tready: in std_logic;
         
         -- input from wishbone adapter 
         s_cc_wb_m2s_valid: in std_logic;
         s_cc_wb_m2s: in t_wishbone_a32_d32_s2m;
--         s_cq_wb_s2m_tready: out std_logic;
         
         -- interface from op/cycle fifo
         m_op_wb_m2s_tvalid: in std_logic;         
         m_op_wb_m2s_tdata:  in std_logic_vector(2 downto 0);
         m_op_wb_s2m_tready: out std_logic
    );
end component pcie3_wb_to_tlp_fifo;

component pcie3_tlp_to_wb_fifo is
    generic(
       g_asynchronous: boolean:= false       
    );
    Port ( clk_i : in STD_LOGIC;
         rst_n_i : in STD_LOGIC;
         
         clk2_i: in std_logic;
         rst2_n_i: in std_logic;
         
         -- wishbone fifo input
         s_cq_wb_s2m: in t_wishbone_a32_d32_m2s;
         s_cq_wb_s2m_tuser: in std_logic_vector(2 downto 0);
         s_cq_wb_m2s_tready: out std_logic;
         
         -- wishbone interface
         m_cq_wb_m2s: out t_wishbone_a32_d32_m2s;
         m_cq_wb_s2m_tready: in std_logic;
         
         -- interface to op/cycle fifo
         m_op_wb_m2s_tvalid: out std_logic;         
         m_op_wb_m2s_tdata:  out std_logic_vector(2 downto 0);
         m_op_wb_s2m_tready: in std_logic
    );
end component pcie3_tlp_to_wb_fifo;

component pcie3_tlp_wb_adapter is
  generic (
    g_outstanding_max: natural:= 32
  );
  Port ( 
    clk_i: in std_logic;
    rst_n_i: in std_logic;
    
    s_cq_wb_m2s: in t_wishbone_a32_d32_m2s;
    s_cq_wb_s2m_tready: out std_logic;
    
    s_cc_wb_s2m: out t_wishbone_a32_d32_s2m;
    s_cc_wb_s2m_tvalid: out std_logic;

  
    -- Wishbone master interface
    m_wb_m2s: out t_wishbone_a32_d32_m2s;
    m_wb_s2m: in  t_wishbone_a32_d32_s2m
  );
end component pcie3_tlp_wb_adapter;

component pcie3_tlp_pending is
  generic (
    g_depth: natural:= 1
  );
  port (
    clk_i: in std_logic;
    rst_n_i: in std_logic;
    -- cq header
    s_m2s_tvalid: in std_logic;
    s_m2s_tdata:  in t_pending_packet;
    s_s2m_tready: out std_logic;
    
    m_m2s_tvalid: out std_logic;
    m_m2s_tdata:  out t_pending_packet;
    m_s2m_tready: in std_logic
   );
end component pcie3_tlp_pending;

end pcie3_mm_components_pkg;

package body pcie3_mm_components_pkg is


end package body  pcie3_mm_components_pkg;
