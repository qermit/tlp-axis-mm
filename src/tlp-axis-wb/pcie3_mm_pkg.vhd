-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : pcie3_mm_pkg.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;

package pcie3_mm_pkg is

  constant c_wishbone_a32_width : integer := 32;
  constant c_wishbone_a64_width : integer := 32;
  constant c_wishbone_d32_width : integer := 32;
  constant c_wishbone_d64_width : integer := 32;


  subtype t_wishbone_a32 is
    std_logic_vector(c_wishbone_a32_width-1 downto 0);
  subtype t_wishbone_d32_select is
    std_logic_vector((c_wishbone_d32_width/8)-1 downto 0);
  subtype t_wishbone_d32 is
    std_logic_vector(c_wishbone_d32_width-1 downto 0); 
       
  subtype t_wishbone_d64_select is
    std_logic_vector((c_wishbone_d64_width/8)-1 downto 0);    
  subtype t_wishbone_d64 is
    std_logic_vector(c_wishbone_d64_width-1 downto 0);


  type t_wishbone_a32_d64_m2s is record
    cyc : std_logic;
    stb : std_logic;
    adr : t_wishbone_a32;
    sel : t_wishbone_d64_select;
    we  : std_logic;
    dat : t_wishbone_d64;
  end record t_wishbone_a32_d64_m2s;

  type t_wishbone_a32_d64_s2m is record
    ack   : std_logic;
    err   : std_logic;
    rty   : std_logic;
    stall : std_logic;
    dat : t_wishbone_d64;
  end record t_wishbone_a32_d64_s2m;
  

  type t_wishbone_a32_d32_m2s is record
    cyc : std_logic;
    stb : std_logic;
    adr : t_wishbone_a32;
    sel : t_wishbone_d32_select;
    we  : std_logic;
    dat : t_wishbone_d32;
  end record t_wishbone_a32_d32_m2s;

  type t_wishbone_a32_d32_s2m is record
    ack   : std_logic;
    err   : std_logic;
    rty   : std_logic;
    stall : std_logic;
    dat : t_wishbone_d32;
  end record t_wishbone_a32_d32_s2m;




  
  component wb_a32_d64_to_a32_d32
  port (
    clk_sys_i: in std_logic;
    rst_n_i: in std_logic;
    
    s_wb_m2s: t_wishbone_a32_d64_m2s;
    s_wb_s2m: t_wishbone_a32_d64_s2m;
    
    m_wb_m2s: t_wishbone_a32_d32_m2s;
    m_wb_s2m: t_wishbone_a32_d32_s2m
  );
  end component wb_a32_d64_to_a32_d32;


  component wb_a32_d32_to_a32_d64
  port (
    clk_sys_i: in std_logic;
    rst_n_i: in std_logic;
    
    s_wb_m2s: t_wishbone_a32_d32_m2s;
    s_wb_s2m: t_wishbone_a32_d32_s2m;
    
    m_wb_m2s: t_wishbone_a32_d64_m2s;
    m_wb_s2m: t_wishbone_a32_d64_s2m
  );
  end component wb_a32_d32_to_a32_d64;
 

end package pcie3_mm_pkg;

package body pcie3_mm_pkg is

end package body pcie3_mm_pkg;
