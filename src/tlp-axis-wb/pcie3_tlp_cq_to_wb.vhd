-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : pcie3_tlp_cq_to_wb.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.pcie_gen3_pkg.all;
use work.pcie3_mm_pkg.all;


entity pcie3_tlp_cq_to_wb is
  generic (
    g_tdata_width: natural:= 128
  );
  Port ( clk_i : in STD_LOGIC;
         rst_n_i : in STD_LOGIC;
           
         s_axis_cq_m2s: in  t_axis_cq_m2s;
         s_axis_cq_s2m: out t_axis_cq_s2m; 
         
         m_wb_m2s: out t_wishbone_a32_d32_m2s;
         m_wb_m2s_tuser: out std_logic_vector(2 downto 0);
         m_wb_s2m_tready: in std_logic;
                  
         m_fifo_m2s_tvalid: out std_logic;
         m_fifo_m2s_tdata:  out t_pending_packet;
         m_fifo_s2m_tready: in std_logic
  );
end pcie3_tlp_cq_to_wb;

architecture Behavioral of pcie3_tlp_cq_to_wb is
  type   t_packet_state is (PKT_STATE_RESET, PKT_STATE_IDLE, PKT_MASTER_STALL, PKT_STATE_IN_PROGRESS, PKT_READ_IN_PROGRESS);
  signal r_packet_state: t_packet_state := PKT_STATE_RESET;
  signal r_read_packet: std_logic:='0';
  
  signal s_s_header: t_axis_cq_header;  
  signal s_s_tuser: t_axis_cq_tuser;
  
  signal r_cq_header: t_axis_cq_header;
  signal r_cq_tuser: t_axis_cq_tuser;  
  signal r_cq_tdata: std_logic_vector(255 downto 0);
  
  signal r_dword_left: unsigned(10 downto 0);
  signal r_dword_current: unsigned(2 downto 0);
    
  signal r_wb_addr : std_logic_vector(31 downto 0);
  
  signal s_wb_we : std_logic;
  signal s_wb_eop : std_logic;
  signal s_wb_sop : std_logic;

  constant c_zeros: std_logic_vector(31 downto 0):= (others => '0');

begin
  s_s_header <= pcie_cq_header_to_type(s_axis_cq_m2s.tdata(127 downto 0));
  s_s_tuser <= f_pcie_cq_tuser_to_type(s_axis_cq_m2s.tuser);
 
  
  s_axis_cq_s2m.tready <= '1' when r_packet_state = PKT_STATE_IDLE or (r_packet_state = PKT_STATE_IN_PROGRESS and r_dword_current = 0  and r_read_packet='0') else '0';
  -- wishbone signals assigment -- 
  m_wb_m2s.dat <= r_cq_tdata(31+32*7 downto 32*7) when r_dword_current = 0 else 
                  r_cq_tdata(31+32*6 downto 32*6) when r_dword_current = 1 else
                  r_cq_tdata(31+32*5 downto 32*5) when r_dword_current = 2 else
                  r_cq_tdata(31+32*4 downto 32*4) when r_dword_current = 3 else
                  r_cq_tdata(31+32*3 downto 32*3) when r_dword_current = 4 else
                  r_cq_tdata(31+32*2 downto 32*2) when r_dword_current = 5 else
                  r_cq_tdata(31+32*1 downto 32*1) when r_dword_current = 6 else
                  r_cq_tdata(31+32*0 downto 32*0) when r_dword_current = 7 else
                  x"DEADBEEF";
                  
  m_wb_m2s.sel <= r_cq_tuser.byte_en(3+4*7 downto 4*7) when r_dword_current = 0 else
                  r_cq_tuser.byte_en(3+4*6 downto 4*6) when r_dword_current = 1 else
                  r_cq_tuser.byte_en(3+4*5 downto 4*5) when r_dword_current = 2 else
                  r_cq_tuser.byte_en(3+4*4 downto 4*4) when r_dword_current = 3 else
                  r_cq_tuser.byte_en(3+4*3 downto 4*3) when r_dword_current = 4 else
                  r_cq_tuser.byte_en(3+4*2 downto 4*2) when r_dword_current = 5 else
                  r_cq_tuser.byte_en(3+4*1 downto 4*1) when r_dword_current = 6 else
                  r_cq_tuser.byte_en(3+4*0 downto 4*0) when r_dword_current = 7 else
                  "0000";
                  
  m_wb_m2s.adr <= r_wb_addr;
  m_wb_m2s.we <= s_wb_we;
  
  m_wb_m2s.cyc <= '1' when r_packet_state = PKT_MASTER_STALL or r_packet_state = PKT_STATE_IN_PROGRESS else '0';
  m_wb_m2s.stb <= '1' when r_packet_state = PKT_STATE_IN_PROGRESS  else '0';
  m_wb_m2s_tuser <= s_wb_we & s_wb_sop & s_wb_eop;
  -- wishbone signals assigment ends here --
  
  
  
  s_wb_we <= '1' when r_cq_header.req_type = pcie_req_mw  else '0';
  s_wb_eop <= '1' when r_dword_left = 1 else '0';
--  s_wb_sop <= r_cq_tuser.sop;

  -- wishbone transfer/op assigment starts here 
  -- wishbone transfer/op assigment ends here
  
  m_fifo_m2s_tdata.attr              <= r_cq_header.attr;
  m_fifo_m2s_tdata.transaction_class <= r_cq_header.transaction_class; 
  m_fifo_m2s_tdata.tag               <= r_cq_header.tag;
  m_fifo_m2s_tdata.requester_id      <= r_cq_header.requester_id;
  m_fifo_m2s_tdata.dword_count       <= r_cq_header.dword_count;
  m_fifo_m2s_tdata.total_byte_count  <= f_pcie_calc_total_byte_count(r_cq_tuser.first_be, r_cq_tuser.last_be, r_cq_header.dword_count);
  m_fifo_m2s_tdata.address_type      <= r_cq_header.address_type;
  m_fifo_m2s_tdata.address           <= r_cq_header.address(6 downto 2) & f_pcie_ferst_be_to_lower_address(r_cq_tuser.first_be);
        
        
p1: process(clk_i)
begin
  if rising_edge(clk_i) then
    case r_packet_state is
      when PKT_STATE_RESET =>
        m_fifo_m2s_tvalid <= '0';
        r_read_packet <= '0';
        if m_fifo_s2m_tready = '1' then
          r_packet_state <= PKT_STATE_IDLE;
        end if;
      when PKT_STATE_IDLE =>
        if s_axis_cq_m2s.tvalid = '1' then
          if s_s_header.req_type = pcie_req_mr then
            m_fifo_m2s_tvalid <= '1';
            r_read_packet <= '1';
--            r_packet_state <= PKT_READ_IN_PROGRESS;
          else
            r_read_packet <= '0';
--            r_packet_state <= PKT_STATE_IN_PROGRESS;
          end if;
            r_packet_state <= PKT_STATE_IN_PROGRESS;
          s_wb_sop <= '1';
         
          
          r_dword_left <= unsigned(s_s_header.dword_count);
          r_dword_current <= to_unsigned(3, 3);
          
          r_cq_tdata  <= s_axis_cq_m2s.tdata;
          r_cq_header <= s_s_header;
          r_cq_tuser  <= s_s_tuser;

          -- assign address and mask bits according to BAR aperture
          r_wb_addr   <= s_s_header.address(31 downto 0);
          if unsigned(s_s_header.bar_aperture) < to_unsigned(32, s_s_header.bar_aperture'length) then
            r_wb_addr(r_wb_addr'left downto to_integer(unsigned(s_s_header.bar_aperture)))  <= c_zeros(r_wb_addr'left downto to_integer(unsigned(s_s_header.bar_aperture)));
          end if;
        end if;
      when PKT_MASTER_STALL =>
        if s_axis_cq_m2s.tvalid = '1' then
          r_cq_tdata  <= s_axis_cq_m2s.tdata;
          r_cq_tuser  <= s_s_tuser;          
        end if;
      when PKT_STATE_IN_PROGRESS =>
        m_fifo_m2s_tvalid <= '0';
--        s_axis_cq_s2m.tready <= '0';
        if m_wb_s2m_tready = '1' then
          s_wb_sop <= '0';

          r_wb_addr <= std_logic_vector(unsigned(r_wb_addr) + 4);
          if r_dword_left /= 0 then
            r_dword_left <= r_dword_left - 1;
          end if;          
          r_packet_state <= PKT_STATE_IN_PROGRESS;
          
          if r_dword_left = 1 or r_dword_left = 0  then
            r_packet_state <= PKT_STATE_RESET;
          else 
            if r_dword_current = 0 then              
              r_dword_current <= to_unsigned(7, r_dword_current'length);
              if r_read_packet = '0' then
                if s_axis_cq_m2s.tvalid = '0' then
                  r_packet_state <= PKT_MASTER_STALL;
                else
                  r_cq_tdata  <= s_axis_cq_m2s.tdata;
                  r_cq_tuser  <= s_s_tuser;  
                end if;
              end if;
            else
              r_dword_current <= r_dword_current - 1;
            end if;
          end if;
        end if;
--      when PKT_STATE_IN_PROGRESS =>
--        m_fifo_m2s_tvalid <= '0';
        

      when others => 
        r_packet_state <= PKT_STATE_RESET;
    end case;
 
    if rst_n_i = '0' then
      r_packet_state <= PKT_STATE_RESET;
      m_fifo_m2s_tvalid <= '0';
      r_wb_addr <= (others => '0');
                s_wb_sop <= '0';

      r_read_packet <= '0';
      r_cq_header <= c_axis_cq_header_zero;
      r_cq_tuser <= c_axis_cq_tuser_zero;  
      r_cq_tdata <= (others => '0');
  
      r_dword_left <= (others => '0');
      r_dword_current <= (others => '0');
    end if;
  end if;
end process;

end Behavioral;
