-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : pcie3_tlp_fifo.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

Library xpm;
use xpm.vcomponents.all;

entity pcie3_tlp_fifo is
  generic (
    g_tdata_width: natural  range 8 to 2048:= 128;
    g_depth: natural range 16 to 4194304:= 16;
    g_async: boolean:=false
  );
  port (
    clk_i: in std_logic;
    rst_n_i: in std_logic;

    clk2_i: in std_logic;
    rst2_n_i: in std_logic;
    -- cq header
    s_m2s_tvalid: in std_logic;
    s_m2s_tdata:  in std_logic_vector(g_tdata_width-1 downto 0);
    s_s2m_tready: out std_logic;
    
    m_m2s_tvalid: out std_logic;
    m_m2s_tdata:  out std_logic_vector(g_tdata_width-1 downto 0);
    m_s2m_tready: in std_logic
   );
end pcie3_tlp_fifo;

architecture Behavioral of pcie3_tlp_fifo is

type   t_state is (STATE_EMPTY, STATE_HAS_DATA, STATE_INIT);
signal r_state: t_state:= STATE_INIT;  

function f_clocking_mode(p_asyc: boolean) return string is
begin
  if p_asyc = true then
    return "independent_clock";
  else
    return "common_clock";  
  end if;
end function;

constant c_clocking_mode:string := f_clocking_mode(g_async);
constant c_FIFO_MEMORY_TYPE: string := "distributed";
-- |   "auto"- Allow Vivado Synthesis to choose                                                                          |
-- |   "block"- Block RAM FIFO                                                                                           |
-- |   "distributed"- Distributed RAM FIFO                                                                               |
-- |   "ultra"- URAM FIFO    
begin

ASYNC_FIFO: if g_async = true generate
  signal s_rst : std_logic;
  signal s_full: std_logic;
  signal s_rst_busy: std_logic;
  signal s_empty: std_logic;
begin
s_rst <= not rst_n_i;
s_s2m_tready <= '1' when  s_full = '0' and s_rst_busy = '0' else '0';
--s_data_valid <= '0'; -- m_m2s_tvalid


  xpm_fifo_async_inst : xpm_fifo_async
   generic map (
      CDC_SYNC_STAGES => 2,       -- DECIMAL
      DOUT_RESET_VALUE => "0",    -- String
      ECC_MODE => "no_ecc",       -- String
      FIFO_MEMORY_TYPE => "auto", -- String
      FIFO_READ_LATENCY => 1,     -- DECIMAL
      FIFO_WRITE_DEPTH => g_depth,   -- DECIMAL
      FULL_RESET_VALUE => 0,      -- DECIMAL
      PROG_EMPTY_THRESH => 10,    -- DECIMAL
      PROG_FULL_THRESH => 10,     -- DECIMAL
      RD_DATA_COUNT_WIDTH => 1,   -- DECIMAL
      READ_DATA_WIDTH => g_tdata_width,      -- DECIMAL
      READ_MODE => "fwft",        -- String
      RELATED_CLOCKS => 0,        -- DECIMAL
      SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
      USE_ADV_FEATURES => "1707", -- String
      WAKEUP_TIME => 0,           -- DECIMAL
      WRITE_DATA_WIDTH => g_tdata_width,     -- DECIMAL
      WR_DATA_COUNT_WIDTH => 1    -- DECIMAL
   )
   port map (
      almost_empty => open,
      almost_full => open,
      data_valid => m_m2s_tvalid,
      dbiterr => open,
      dout => m_m2s_tdata,
      empty => s_empty,
      full => s_full,
      overflow => open,
      prog_empty => open,
      prog_full => open,
      rd_data_count => open,
      rd_rst_busy => open,
      sbiterr => open,
      underflow => open,
      wr_ack => open,
      wr_data_count => open,
      wr_rst_busy => s_rst_busy,
      din => s_m2s_tdata,
      injectdbiterr => '0',
      injectsbiterr => '0',
      rd_clk => clk2_i,
      rd_en => m_s2m_tready,
      rst => s_rst,
      sleep => '0',
      wr_clk => clk_i,
      wr_en => s_m2s_tvalid
   );

end generate ASYNC_FIFO;

SYNC_FIFO: if g_async = false  generate
  signal s_rst : std_logic;
  signal s_full: std_logic;
  signal s_rst_busy: std_logic;
begin
s_rst <= not rst_n_i;
s_s2m_tready <= '1' when  s_full = '0' and s_rst_busy = '0' else '0';

  xpm_fifo_sync_inst : xpm_fifo_sync
   generic map (
      DOUT_RESET_VALUE => "0",    -- String
      ECC_MODE => "no_ecc",       -- String
      FIFO_MEMORY_TYPE => "auto", -- String
      FIFO_READ_LATENCY => 0,     -- DECIMAL
      FIFO_WRITE_DEPTH => g_depth,-- DECIMAL
      FULL_RESET_VALUE => 0,      -- DECIMAL
      PROG_EMPTY_THRESH => 10,    -- DECIMAL
      PROG_FULL_THRESH => 10,     -- DECIMAL
      RD_DATA_COUNT_WIDTH => 1,   -- DECIMAL
      READ_DATA_WIDTH => g_tdata_width,      -- DECIMAL
      READ_MODE => "fwft",         -- String std/fwft
      SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
      USE_ADV_FEATURES => "1707", -- String
      WAKEUP_TIME => 0,           -- DECIMAL
      WRITE_DATA_WIDTH => g_tdata_width,     -- DECIMAL
      WR_DATA_COUNT_WIDTH => 1    -- DECIMAL
   )
   port map (
      almost_empty => open,
      almost_full => open,
      data_valid => m_m2s_tvalid,
      dbiterr => open,
      dout => m_m2s_tdata,
      empty => open,
      full => s_full,
      overflow => open,
      prog_empty => open,
      prog_full => open,
      rd_data_count => open,
      rd_rst_busy => open,
      sbiterr => open,
      underflow => open,
      wr_ack => open,
      wr_data_count => open,
      wr_rst_busy => s_rst_busy,
      din => s_m2s_tdata,
      injectdbiterr => '0',
      injectsbiterr => '0',
      rd_en => m_s2m_tready,
      rst => s_rst,
      sleep => '0',
      wr_clk => clk_i,
      wr_en => s_m2s_tvalid
   );

end generate SYNC_FIFO;

end Behavioral;
