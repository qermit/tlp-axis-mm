-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : pcie3_tlp_pending.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.pcie_gen3_pkg.all;
use work.pcie3_mm_components_pkg.all;


entity pcie3_tlp_pending is
  generic (
    g_depth: natural:= 1
  );
  port (
    clk_i: in std_logic;
    rst_n_i: in std_logic;
    -- cq header
    s_m2s_tvalid: in std_logic;
    s_m2s_tdata:  in t_pending_packet;
    s_s2m_tready: out std_logic;
    
    m_m2s_tvalid: out std_logic;
    m_m2s_tdata:  out t_pending_packet;
    m_s2m_tready: in std_logic
   );
end pcie3_tlp_pending;

architecture Behavioral of pcie3_tlp_pending is
  constant c_pending_fifo_width: natural := 63;
  
  signal s_pending_fifo_tdata: std_logic_vector(c_pending_fifo_width - 1 downto 0);
  signal m_pending_fifo_tdata: std_logic_vector(c_pending_fifo_width - 1 downto 0);

begin

u_pending_fifo: pcie3_tlp_fifo 
  generic map (
    g_tdata_width => c_pending_fifo_width
  )
  port map (
    clk_i => clk_i,
    rst_n_i => rst_n_i,
    
    clk2_i => clk_i,
    rst2_n_i => rst_n_i,
    -- cq header
    s_m2s_tvalid => s_m2s_tvalid,
    s_m2s_tdata => s_pending_fifo_tdata,
    s_s2m_tready => s_s2m_tready,
    
    m_m2s_tvalid => m_m2s_tvalid,
    m_m2s_tdata => m_pending_fifo_tdata,
    m_s2m_tready => m_s2m_tready
   );
--   m_pending_fifo_tdata


   s_pending_fifo_tdata(62 downto 60) <= s_m2s_tdata.attr;
   s_pending_fifo_tdata(59 downto 57) <= s_m2s_tdata.transaction_class;
   s_pending_fifo_tdata(56 downto 49) <= s_m2s_tdata.tag;
   s_pending_fifo_tdata(48 downto 41) <= s_m2s_tdata.requester_id.device_function; -- : std_logic_vector (7 downto 0);
   s_pending_fifo_tdata(40 downto 33) <= s_m2s_tdata.requester_id.bus_id; --: std_logic_vector(7 downto 0);
   s_pending_fifo_tdata(32 downto 22) <= s_m2s_tdata.dword_count;
   s_pending_fifo_tdata(21 downto 9) <= s_m2s_tdata.total_byte_count;
   s_pending_fifo_tdata(8 downto 7) <= s_m2s_tdata.address_type;
   s_pending_fifo_tdata(6 downto 0) <= s_m2s_tdata.address;

   m_m2s_tdata.attr <= m_pending_fifo_tdata(62 downto 60);
   m_m2s_tdata.transaction_class <= m_pending_fifo_tdata(59 downto 57);
   m_m2s_tdata.tag <= m_pending_fifo_tdata(56 downto 49);
   m_m2s_tdata.requester_id.device_function <= m_pending_fifo_tdata(48 downto 41);
   m_m2s_tdata.requester_id.bus_id <= m_pending_fifo_tdata(40 downto 33);
   m_m2s_tdata.dword_count <= m_pending_fifo_tdata(32 downto 22);
   m_m2s_tdata.total_byte_count <= m_pending_fifo_tdata(21 downto 9);
   m_m2s_tdata.address_type <= m_pending_fifo_tdata(8 downto 7);
   m_m2s_tdata.address <= m_pending_fifo_tdata(6 downto 0);
end Behavioral;
