-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : pcie3_tlp_pending_fifo.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity pcie3_tlp_pending_fifo is
  generic (
    g_tdata_width: natural:= 128;
    g_depth: natural:= 1
  );
  port (
    clk_i: in std_logic;
    rst_n_i: in std_logic;
    -- cq header
    s_m2s_tvalid: in std_logic;
    s_m2s_tdata:  in std_logic_vector(g_tdata_width-1 downto 0);
    s_s2m_tready: out std_logic;
    
    m_m2s_tvalid: out std_logic;
    m_m2s_tdata:  out std_logic_vector(g_tdata_width-1 downto 0);
    m_s2m_tready: in std_logic
   );
end pcie3_tlp_pending_fifo;

architecture Behavioral of pcie3_tlp_pending_fifo is

type   t_state is (STATE_EMPTY, STATE_HAS_DATA, STATE_INIT);
signal r_state: t_state:= STATE_INIT;  

begin

s_s2m_tready <= '1' when r_state = STATE_EMPTY else
                '0';

m_m2s_tvalid <= '1' when r_state = STATE_HAS_DATA else
                '0'; 


p1: process(clk_i)
begin
  if rising_edge(clk_i) then
    case r_state is 
      when STATE_EMPTY =>
        if s_m2s_tvalid = '1' then
          m_m2s_tdata <= s_m2s_tdata;
          r_state <= STATE_HAS_DATA;
        end if;
      when STATE_HAS_DATA =>
        if m_s2m_tready = '1' then
          r_state <= STATE_EMPTY;
        end if;
      when others => 
        r_state <= STATE_EMPTY;                
    end case;
    
    if rst_n_i = '0' then
      r_state <= STATE_INIT;
      m_m2s_tdata <= (others => '0');
    end if;
  end if;
end process;

end Behavioral;
