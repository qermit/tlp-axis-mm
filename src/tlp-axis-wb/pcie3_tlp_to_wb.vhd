-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : pcie3_tlp_to_wb.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.pcie_gen3_pkg.all;
use work.pcie3_mm_pkg.all;
use work.pcie3_mm_components_pkg.all;


entity pcie3_tlp_to_wb is
    generic(
       wb_data_width: natural := 64;
       g_asynchronous: boolean:= false       
    );
    Port ( clk_i   : in STD_LOGIC;
           rst_n_i : in STD_LOGIC;
           
           clk2_i : in std_logic;
           rst2_n_i: in std_logic;
           
           -- slave completer request interface
           s_axis_cq_m2s: in  t_axis_cq_m2s;
           s_axis_cq_s2m: out t_axis_cq_s2m;
           -- master completer completion interface           
           m_axis_cc_m2s: out t_axis_cc_m2s;
           m_axis_cc_s2m: in  t_axis_cc_s2m;
           
           -- Wishbone master interface
           m_wb_m2s: out t_wishbone_a32_d32_m2s;
           m_wb_s2m: in  t_wishbone_a32_d32_s2m
                        
           );
end pcie3_tlp_to_wb;

architecture Behavioral of pcie3_tlp_to_wb is


constant c_pending_fifo_width: natural := 128;

signal m_req_wb_m2s: t_wishbone_a32_d32_m2s;
signal m_req_wb_m2s_tuser: std_logic_vector(2 downto 0); 
signal m_req_wb_s2m_tready: std_logic;


signal m_compl_wb_s2m: t_wishbone_a32_d32_s2m;
signal m_compl_wb_s2m_tuser: std_logic_vector(2 downto 0); 
signal m_compl_wb_s2m_tvalid: std_logic; 
signal m_compl_wb_m2s_tready: std_logic;

-- pendif fifo
signal s_pending_fifo_tvalid: std_logic;
signal s_pending_fifo_tdata: t_pending_packet;
signal s_pending_fifo_tready: std_logic;

signal s_fifo_m2s_tvalid:  std_logic;
signal s_fifo_m2s_tdata:   t_pending_packet;
signal s_fifo_s2m_tready:  std_logic;


-- pendif fifo
signal s_op_w_fifo_tvalid: std_logic;
signal s_op_w_fifo_tdata: std_logic_vector(2 downto 0);
signal s_op_w_fifo_tready: std_logic;

signal s_op_r_fifo_tvalid:  std_logic;
signal s_op_r_fifo_tdata:   std_logic_vector(2 downto 0);
signal s_op_r_fifo_tready:  std_logic;


signal s_adapter_cq_wb_m2s: t_wishbone_a32_d32_m2s;
signal s_adapter_cq_wb_s2m_tready:  std_logic;
    
signal s_adapter_cc_wb_s2m: t_wishbone_a32_d32_s2m;
signal s_adapter_cc_wb_s2m_tvalid :  std_logic;

signal s_clk_out: std_logic;
signal s_rst_out_n: std_logic;

begin

ASYNC_OUT_CLOCK: if g_asynchronous = true generate
  s_clk_out <= clk2_i;
  s_rst_out_n <= rst2_n_i;
end generate ASYNC_OUT_CLOCK;

SYNC_OUT_CLOCK: if g_asynchronous = false generate
  s_clk_out <= clk_i;
  s_rst_out_n <= rst_n_i;
end generate SYNC_OUT_CLOCK;

-- receiver
u_completer_req: pcie3_tlp_cq_to_wb 
  Port map ( clk_i => clk_i,
         rst_n_i => rst_n_i,
           
         s_axis_cq_m2s => s_axis_cq_m2s,
         s_axis_cq_s2m => s_axis_cq_s2m, 
         
         m_wb_m2s => m_req_wb_m2s,
         m_wb_m2s_tuser => m_req_wb_m2s_tuser, 
         m_wb_s2m_tready => m_req_wb_s2m_tready,
         
         -- pending reqiest fifo
         m_fifo_m2s_tvalid => s_pending_fifo_tvalid,
         m_fifo_m2s_tdata => s_pending_fifo_tdata,
         m_fifo_s2m_tready => s_pending_fifo_tready         
  );


 u_tlp_to_wb_fifo: pcie3_tlp_to_wb_fifo 
     generic map (
      g_asynchronous => g_asynchronous
    )
    Port map ( clk_i => clk_i,
         rst_n_i => rst_n_i,
         
         clk2_i => s_clk_out,
         rst2_n_i => s_rst_out_n,
         
         -- wishbone fifo input
         s_cq_wb_s2m => m_req_wb_m2s,
         s_cq_wb_s2m_tuser => m_req_wb_m2s_tuser,
         s_cq_wb_m2s_tready => m_req_wb_s2m_tready,
         
         -- wishbone interface
         m_cq_wb_m2s => s_adapter_cq_wb_m2s,
         m_cq_wb_s2m_tready => s_adapter_cq_wb_s2m_tready,
         
         -- interface to op/cycle fifo
         m_op_wb_m2s_tvalid=> s_op_w_fifo_tvalid,         
         m_op_wb_m2s_tdata => s_op_w_fifo_tdata,
         m_op_wb_s2m_tready => s_op_w_fifo_tready
    );

-- fifo
u_pending_fifo: pcie3_tlp_pending
  generic map (
    g_depth => 2
  )
  port map (
    clk_i => clk_i,
    rst_n_i => rst_n_i,
    -- cq header
    s_m2s_tvalid => s_pending_fifo_tvalid,
    s_m2s_tdata => s_pending_fifo_tdata,
    s_s2m_tready => s_pending_fifo_tready,
    
    m_m2s_tvalid => s_fifo_m2s_tvalid,
    m_m2s_tdata => s_fifo_m2s_tdata,
    m_s2m_tready => s_fifo_s2m_tready
   );



-- completer
u_completer_compl : pcie3_tlp_wb_to_cc
  generic map (
     g_tdata_width => c_pending_fifo_width
  )
  Port map (
    clk_i => clk_i,
    rst_n_i => rst_n_i,
           
         s_axis_cc_m2s => m_axis_cc_m2s,
         s_axis_cc_s2m => m_axis_cc_s2m,
          
         -- wishbone return fifo
         s_wb_s2m => m_compl_wb_s2m,
         s_wb_s2m_tuser => m_compl_wb_s2m_tuser,
         s_wb_s2m_tvalid => m_compl_wb_s2m_tvalid,
         s_wb_m2s_tready => m_compl_wb_m2s_tready,
         
        -- pending requests fifo       
         s_fifo_m2s_tvalid => s_fifo_m2s_tvalid,
         s_fifo_m2s_tdata => s_fifo_m2s_tdata,
         s_fifo_s2m_tready => s_fifo_s2m_tready
   );
   
  
  u_wb_to_tlp_fifo: pcie3_wb_to_tlp_fifo 
    generic map (
      g_asynchronous => g_asynchronous
    )
    Port map (   
       clk_i => clk_i,
       rst_n_i => rst_n_i,
       
       clk_wb_i => s_clk_out,
         rst_wb_n_i => s_rst_out_n,
         
         -- output to cc
         m_cc_wb_s2m => m_compl_wb_s2m,
         m_cc_wb_s2m_tuser => m_compl_wb_s2m_tuser,
         m_cc_wb_s2m_tvalid => m_compl_wb_s2m_tvalid,
         m_cc_wb_s2m_tready => m_compl_wb_m2s_tready,
         
         -- input from wishbone adapter 
         s_cc_wb_m2s_valid => s_adapter_cc_wb_s2m_tvalid,
         s_cc_wb_m2s       => s_adapter_cc_wb_s2m,
         
         -- interface from op/cycle fifo
         m_op_wb_m2s_tvalid => s_op_r_fifo_tvalid,
         m_op_wb_m2s_tdata  => s_op_r_fifo_tdata,
         m_op_wb_s2m_tready => s_op_r_fifo_tready
    );
    
  -- op fifo, to track type of data

b_op_fifo: block
signal s_op_w_fifo_tdata_resized: std_logic_vector(7 downto 0) := (others => '0');
signal s_op_r_fifo_tdata_resized: std_logic_vector(7 downto 0) := (others => '0');
begin
s_op_w_fifo_tdata_resized(2 downto 0) <= s_op_w_fifo_tdata;
s_op_r_fifo_tdata <= s_op_r_fifo_tdata_resized(2 downto 0);

u_op_fifo: pcie3_tlp_fifo 
  generic map (
    g_tdata_width => 8
  )
  port map (
    clk_i => clk_i,
    rst_n_i => rst_n_i,

    clk2_i => clk_i,
    rst2_n_i => rst_n_i,
        
    -- cq header
    s_m2s_tvalid => s_op_w_fifo_tvalid,
    s_m2s_tdata => s_op_w_fifo_tdata_resized,
    s_s2m_tready => s_op_w_fifo_tready,
    
    m_m2s_tvalid => s_op_r_fifo_tvalid,
    m_m2s_tdata => s_op_r_fifo_tdata_resized,
    m_s2m_tready => s_op_r_fifo_tready
   );
end block; 
  
  u_tlp_wb_adapter: pcie3_tlp_wb_adapter 
  port map (
    clk_i => s_clk_out,
    rst_n_i => s_rst_out_n,

    s_cq_wb_m2s => s_adapter_cq_wb_m2s,
    s_cq_wb_s2m_tready => s_adapter_cq_wb_s2m_tready,
    
    s_cc_wb_s2m => s_adapter_cc_wb_s2m ,
    s_cc_wb_s2m_tvalid => s_adapter_cc_wb_s2m_tvalid,
  
    -- Wishbone master interface
    m_wb_m2s => m_wb_m2s,
    m_wb_s2m => m_wb_s2m
  );
   
end Behavioral;
