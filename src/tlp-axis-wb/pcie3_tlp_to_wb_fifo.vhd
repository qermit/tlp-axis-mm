-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : pcie3_tlp_to_wb_fifo.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.pcie3_mm_pkg.all;
use work.pcie3_mm_components_pkg.all;


entity pcie3_tlp_to_wb_fifo is
    generic(
       g_asynchronous: boolean:= false       
    );
    Port ( clk_i : in STD_LOGIC;
         rst_n_i : in STD_LOGIC;
         
         clk2_i: in std_logic;
         rst2_n_i: in std_logic;
         
         -- wishbone fifo input
         s_cq_wb_s2m: in t_wishbone_a32_d32_m2s;
         s_cq_wb_s2m_tuser: in std_logic_vector(2 downto 0);
         s_cq_wb_m2s_tready: out std_logic;
         
         -- wishbone interface
         m_cq_wb_m2s: out t_wishbone_a32_d32_m2s;
         m_cq_wb_s2m_tready: in std_logic;
         
         -- interface to op/cycle fifo
         m_op_wb_m2s_tvalid: out std_logic;         
         m_op_wb_m2s_tdata:  out std_logic_vector(2 downto 0);
         m_op_wb_s2m_tready: in std_logic
    );
end pcie3_tlp_to_wb_fifo;

architecture Behavioral of pcie3_tlp_to_wb_fifo is
 constant c_fifo_width: natural := 32 + 32 + 4 + 1;
 
 signal s_pending_fifo_tvalid: std_logic;
 signal s_pending_fifo_tdata: std_logic_vector(c_fifo_width - 1 downto 0);
 signal s_pending_fifo_tready: std_logic;
 
 signal m_cq_wb_s2m_tvalid: std_logic;
 signal m_cq_wb_s2m_tdata: std_logic_vector(c_fifo_width - 1 downto 0);


 
begin
s_pending_fifo_tdata(31 downto 0) <=  s_cq_wb_s2m.dat;
s_pending_fifo_tdata(63 downto 32) <=  s_cq_wb_s2m.adr;
s_pending_fifo_tdata(67 downto 64) <=  s_cq_wb_s2m.sel;
s_pending_fifo_tdata(68) <=  s_cq_wb_s2m.we;

u_wishbone_fifo: pcie3_tlp_fifo 
  generic map (
    g_tdata_width => c_fifo_width,
    g_async => g_asynchronous
  )
  port map (
    clk_i => clk_i,
    rst_n_i => rst_n_i,
    
    clk2_i => clk2_i,
    rst2_n_i => rst2_n_i,
    -- cq header
    s_m2s_tvalid => s_pending_fifo_tvalid,
    s_m2s_tdata  => s_pending_fifo_tdata,
    s_s2m_tready => s_pending_fifo_tready,
    
    m_m2s_tvalid => m_cq_wb_s2m_tvalid,
    m_m2s_tdata  => m_cq_wb_s2m_tdata,
    m_s2m_tready => m_cq_wb_s2m_tready
   );

-- three-way sync/broadcast
s_cq_wb_m2s_tready <= '1' when s_pending_fifo_tready = '1' and m_op_wb_s2m_tready = '1' else '0';
s_pending_fifo_tvalid <= '1' when s_cq_wb_s2m.cyc = '1' and s_cq_wb_s2m.stb = '1' and m_op_wb_s2m_tready = '1' else '0';
m_op_wb_m2s_tvalid <= '1' when s_cq_wb_s2m.cyc = '1' and s_cq_wb_s2m.stb = '1' and s_pending_fifo_tready = '1' else '0';

m_op_wb_m2s_tdata <= s_cq_wb_s2m_tuser;

m_cq_wb_m2s.dat <= m_cq_wb_s2m_tdata(31 downto 0);
m_cq_wb_m2s.adr <= m_cq_wb_s2m_tdata(63 downto 32);
m_cq_wb_m2s.sel <= m_cq_wb_s2m_tdata(67 downto 64);
m_cq_wb_m2s.we  <= m_cq_wb_s2m_tdata(68);
m_cq_wb_m2s.cyc <= '1' when m_cq_wb_s2m_tvalid = '1' else '0';
m_cq_wb_m2s.stb <= '1' when m_cq_wb_s2m_tvalid = '1' else '0';

end Behavioral;
