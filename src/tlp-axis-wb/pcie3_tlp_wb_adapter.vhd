-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : pcie3_tlp_wb_adapter.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.pcie3_mm_pkg.all;


entity pcie3_tlp_wb_adapter is
  generic (
    g_outstanding_max: natural:= 32
  );
  Port ( 
    clk_i: in std_logic;
    rst_n_i: in std_logic;

  
    s_cq_wb_m2s: in t_wishbone_a32_d32_m2s;
    s_cq_wb_s2m_tready: out std_logic;
    
    s_cc_wb_s2m: out t_wishbone_a32_d32_s2m;
    s_cc_wb_s2m_tvalid: out std_logic;

  
    -- Wishbone master interface
    m_wb_m2s: out t_wishbone_a32_d32_m2s;
    m_wb_s2m: in  t_wishbone_a32_d32_s2m
  );
end pcie3_tlp_wb_adapter;

architecture Behavioral of pcie3_tlp_wb_adapter is
  -- todo: adjust widht with g_outstanding_max
  signal r_outstanding_counter: unsigned(7 downto 0);
  
  signal s_ack: std_logic;
  signal s_cyc: std_logic;
begin
  m_wb_m2s.dat <= s_cq_wb_m2s.dat;
  m_wb_m2s.adr <= s_cq_wb_m2s.adr;
  m_wb_m2s.we <= s_cq_wb_m2s.we;
  m_wb_m2s.stb <= s_cq_wb_m2s.stb;
  m_wb_m2s.sel <= s_cq_wb_m2s.sel;
  m_wb_m2s.cyc <= '1' when s_cq_wb_m2s.cyc = '1' or r_outstanding_counter /= 0 else '0';

  
  s_cq_wb_s2m_tready <= not m_wb_s2m.stall;
   
  
  s_cc_wb_s2m <= m_wb_s2m;
  s_cc_wb_s2m_tvalid <= s_ack;
  
  s_ack <= '1' when m_wb_s2m.ack = '1' or m_wb_s2m.err = '1' or m_wb_s2m.rty = '1' else '0';
  s_cyc <= '1' when m_wb_s2m.stall = '0' and s_cq_wb_m2s.cyc = '1' and s_cq_wb_m2s.stb = '1' else '0';
  
p_left: process(clk_i)
begin
  if rising_edge(clk_i) then
    if s_cyc = '1' and s_ack = '0' then
      r_outstanding_counter <= r_outstanding_counter + 1;
    elsif s_cyc = '0' and s_ack = '1' then
      r_outstanding_counter <= r_outstanding_counter - 1;    
    end if;
    
    if rst_n_i = '0' then
      r_outstanding_counter <= to_unsigned(0, r_outstanding_counter'length);
    end if;
  end if;
end process;

end Behavioral;
