-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : pcie3_tlp_wb_to_cc.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.pcie_gen3_pkg.all;
use work.pcie3_mm_pkg.all;


entity pcie3_tlp_wb_to_cc is
  generic (
     g_tdata_width: natural := 128
  );
  Port ( clk_i : in STD_LOGIC;
         rst_n_i : in STD_LOGIC;
           
         s_axis_cc_m2s: out t_axis_cc_m2s;
         s_axis_cc_s2m: in  t_axis_cc_s2m;
         -- pending requests fifo
          
         -- wishbone return fifo
         s_wb_s2m: in t_wishbone_a32_d32_s2m;
         s_wb_s2m_tuser: in std_logic_vector(2 downto 0);
         s_wb_s2m_tvalid: in std_logic;
         s_wb_m2s_tready: out std_logic;
                  
         s_fifo_m2s_tvalid: in std_logic;
         s_fifo_m2s_tdata:  in t_pending_packet;
         s_fifo_s2m_tready: out std_logic
   );
end pcie3_tlp_wb_to_cc;

architecture Behavioral of pcie3_tlp_wb_to_cc is
  type   t_packet_state is (PKT_STATE_IDLE, PKT_WAIT_FOR_PENDING, PKT_STATE_IN_PROGRESS);
  
  signal r_packet_state: t_packet_state := PKT_STATE_IDLE;
    
  signal r_dword_left: unsigned(10 downto 0);
  signal r_dword_current: unsigned(2 downto 0);
  signal r_was_last: std_logic;
  
  signal r_wait_for_tready: std_logic;
  signal s_wait_for_tready: std_logic;
  
  
  signal s_wb_s2m_tuser_we: std_logic;
  signal s_wb_s2m_tuser_sop: std_logic;
  signal s_wb_s2m_tuser_eop: std_logic;  
  
  signal s_axis_cc_m2s_tvalid: std_logic;
  
  signal s_response_header: std_logic_vector(95 downto 0) := (others => '0');
begin

   
   s_wb_s2m_tuser_we <= s_wb_s2m_tuser(2);
   s_wb_s2m_tuser_sop <= s_wb_s2m_tuser(1);
   s_wb_s2m_tuser_eop <= s_wb_s2m_tuser(0);

s_wait_for_tready <= '1' when s_axis_cc_m2s_tvalid = '1' and s_axis_cc_s2m.tready = '0' else '0'; 
--                     '1' when s_axis_cc_m2s_tvalid = '1' and s_axis_cc_s2m.tready = '0' else '0';
s_wb_m2s_tready <= '1' when r_packet_state = PKT_STATE_IDLE        and s_wait_for_tready = '0'
              else '1' when r_packet_state = PKT_STATE_IN_PROGRESS and s_wait_for_tready = '0'
              else '0';
              

s_response_header(6 downto 0) <= s_fifo_m2s_tdata.address;
s_response_header(28 downto 16) <= s_fifo_m2s_tdata.total_byte_count;
s_response_header(42 downto 32) <= s_fifo_m2s_tdata.dword_count;
s_response_header(45 downto 43) <= "000"; -- successfull completion
s_response_header(46) <= '0'; -- posioned completion 
s_response_header(63 downto 48) <= f_pcie_cc_requester_id_to_val(s_fifo_m2s_tdata.requester_id);
s_response_header(71 downto 64) <= s_fifo_m2s_tdata.tag;
s_response_header(91 downto 89) <= s_fifo_m2s_tdata.transaction_class;
s_response_header(94 downto 92) <= s_fifo_m2s_tdata.attr;


 

p1: process(clk_i)
begin
  if rising_edge(clk_i) then
    case r_packet_state is
      when PKT_STATE_IDLE =>
        s_fifo_s2m_tready <= '0';
        if s_wait_for_tready = '0' then
          s_axis_cc_m2s_tvalid <= '0';
          s_axis_cc_m2s.tvalid <= '0';
          if s_wb_s2m_tvalid = '1' and s_wb_s2m_tuser_we = '0' then
            s_axis_cc_m2s.tdata(127 downto 96) <= s_wb_s2m.dat;
            s_axis_cc_m2s.tkeep <= "00001111";


            if s_fifo_m2s_tvalid = '1' then
              s_axis_cc_m2s.tdata(95 downto 0) <= s_response_header;
              s_fifo_s2m_tready <= '1';
          
              if s_wb_s2m_tuser_eop = '1' then             
                r_packet_state <= PKT_STATE_IDLE;   
                s_axis_cc_m2s_tvalid <= '1';   
                s_axis_cc_m2s.tvalid <= '1';      
              else
                r_packet_state <= PKT_STATE_IN_PROGRESS;
              end if;
            else
              r_packet_state <= PKT_WAIT_FOR_PENDING;          
            end if; 
          
            r_dword_current <= to_unsigned(3,3);
            if s_wb_s2m_tuser_eop = '1' then
              s_axis_cc_m2s.tlast <= '1';            
              r_was_last <= '1';
            else          
              s_axis_cc_m2s.tlast <= '0';
              r_was_last <= '0';
            end if;
          end if;
        end if;
        
      when PKT_WAIT_FOR_PENDING =>
        if s_fifo_m2s_tvalid = '1' then
          s_axis_cc_m2s.tdata(95 downto 0) <= s_response_header;
          s_fifo_s2m_tready <= '1';
          
          if r_was_last = '1' then             
            r_packet_state <= PKT_STATE_IDLE;   
            s_axis_cc_m2s_tvalid <= '1';   
            s_axis_cc_m2s.tvalid <= '1';      
          else
            r_packet_state <= PKT_STATE_IN_PROGRESS;
          end if;
          
        end if;          
      when PKT_STATE_IN_PROGRESS =>
        s_fifo_s2m_tready <= '0';
        if s_wait_for_tready = '0' then
          if s_wb_s2m_tvalid = '1' then
            case r_dword_current is
              when "111" => 
                s_axis_cc_m2s.tdata(0*32+31 downto 0*32) <= s_wb_s2m.dat;
                s_axis_cc_m2s.tkeep <= "00000001";
              when "110" => 
                s_axis_cc_m2s.tdata(1*32+31 downto 1*32) <= s_wb_s2m.dat;
                s_axis_cc_m2s.tkeep <= "00000011";
              when "101" =>
                s_axis_cc_m2s.tdata(2*32+31 downto 2*32) <= s_wb_s2m.dat;
                s_axis_cc_m2s.tkeep <= "00000111";
              when "100" =>
                s_axis_cc_m2s.tdata(3*32+31 downto 3*32) <= s_wb_s2m.dat;
                s_axis_cc_m2s.tkeep <= "00001111";
              when "011" =>
                s_axis_cc_m2s.tdata(4*32+31 downto 4*32) <= s_wb_s2m.dat;
                s_axis_cc_m2s.tkeep <= "00011111";
              when "010" =>
                s_axis_cc_m2s.tdata(5*32+31 downto 5*32) <= s_wb_s2m.dat;
                s_axis_cc_m2s.tkeep <= "00111111";
              when "001" =>
                s_axis_cc_m2s.tdata(6*32+31 downto 6*32) <= s_wb_s2m.dat;
                s_axis_cc_m2s.tkeep <= "01111111";
              when "000" =>
                s_axis_cc_m2s.tdata(7*32+31 downto 7*32) <= s_wb_s2m.dat;
                s_axis_cc_m2s.tkeep <= "11111111";
              when others =>
                
            end case;
            
            if r_dword_current = 0 then
              s_axis_cc_m2s_tvalid <= '1';
              s_axis_cc_m2s.tvalid <= '1';
              r_dword_current <= to_unsigned(7, r_dword_current'length);
            else 
              r_dword_current <= r_dword_current - 1;
            end if;

            if s_wb_s2m_tuser_eop = '1' then
              s_axis_cc_m2s.tlast <= '1';     
              r_was_last <= '1';   
              s_axis_cc_m2s_tvalid <= '1';
              s_axis_cc_m2s.tvalid <= '1';
              r_packet_state <= PKT_STATE_IDLE;
            else
 
              s_axis_cc_m2s.tlast <= '0';
              r_was_last <= '0';
              
            end if;
            
          end if; 
        end if;      
      when others =>
        r_packet_state <= PKT_STATE_IDLE;
    end case;
 
    if rst_n_i = '0' then
      s_axis_cc_m2s <= c_axis_cc_m2s_zero;
      s_fifo_s2m_tready <= '0';
      r_packet_state <= PKT_STATE_IDLE;
      r_wait_for_tready <= '0';
      r_dword_left <= (others => '0');
      r_dword_current <= (others => '0');
    end if;
  end if;
end process;



end Behavioral;