-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : pcie3_tlp_cq_to_wb.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.pcie3_mm_pkg.all;
use work.pcie3_mm_components_pkg.all;


entity pcie3_wb_to_tlp_fifo is
    generic(
       g_asynchronous: boolean:= false       
    );
    Port ( clk_i : in STD_LOGIC;
         rst_n_i : in STD_LOGIC;
                  
         clk_wb_i: in std_logic;
         rst_wb_n_i: in std_logic;
         
         -- output to cc
         m_cc_wb_s2m: out t_wishbone_a32_d32_s2m;
         m_cc_wb_s2m_tuser: out std_logic_vector(2 downto 0);
         m_cc_wb_s2m_tvalid: out std_logic;
         m_cc_wb_s2m_tready: in std_logic;
         
         -- input from wishbone adapter 
         s_cc_wb_m2s_valid: in std_logic;
         s_cc_wb_m2s: in t_wishbone_a32_d32_s2m;
--         s_cq_wb_s2m_tready: out std_logic;
         
         -- interface from op/cycle fifo
         m_op_wb_m2s_tvalid: in std_logic;         
         m_op_wb_m2s_tdata:  in std_logic_vector(2 downto 0);
         m_op_wb_s2m_tready: out std_logic
    );
end pcie3_wb_to_tlp_fifo;

architecture Behavioral of pcie3_wb_to_tlp_fifo is
 constant c_fifo_width: natural := 32 + 3;

 signal  s_pending_fifo_tdata: std_logic_vector(c_fifo_width - 1 downto 0);
 signal m_cq_wb_s2m_tdata: std_logic_vector(c_fifo_width - 1 downto 0);
 signal m_cq_wb_s2m_tready: std_logic;
 signal m_cq_wb_s2m_tvalid: std_logic;
 
 
begin
  s_pending_fifo_tdata(31 downto 0) <= s_cc_wb_m2s.dat;
  s_pending_fifo_tdata(32) <= s_cc_wb_m2s.ack;
  s_pending_fifo_tdata(33) <= s_cc_wb_m2s.err;
  s_pending_fifo_tdata(34) <= s_cc_wb_m2s.rty;
  
u_wishbone_fifo: pcie3_tlp_fifo 
  generic map (
    g_tdata_width => c_fifo_width,
    g_async => g_asynchronous
  )
  port map (
    clk_i => clk_wb_i,
    rst_n_i => rst_wb_n_i,

    clk2_i => clk_i,
    rst2_n_i => rst_n_i,
    
    -- cq header
    s_m2s_tvalid => s_cc_wb_m2s_valid,
    s_m2s_tdata  => s_pending_fifo_tdata,
    s_s2m_tready => open,
    
    m_m2s_tvalid => m_cq_wb_s2m_tvalid,
    m_m2s_tdata  => m_cq_wb_s2m_tdata,
    m_s2m_tready => m_cq_wb_s2m_tready
   );

m_cq_wb_s2m_tready <= '1' when m_cc_wb_s2m_tready = '1' and m_op_wb_m2s_tvalid = '1' else '0';
m_op_wb_s2m_tready <= '1' when m_cc_wb_s2m_tready = '1' and m_cq_wb_s2m_tvalid = '1'  else '0';

m_cc_wb_s2m_tvalid <= '1' when m_cq_wb_s2m_tvalid = '1' and m_op_wb_m2s_tvalid = '1' else '0';

m_cc_wb_s2m.dat  <= m_cq_wb_s2m_tdata(31 downto 0);
m_cc_wb_s2m.ack  <= m_cq_wb_s2m_tdata(32);
m_cc_wb_s2m.err  <= m_cq_wb_s2m_tdata(33); 
m_cc_wb_s2m.rty  <= m_cq_wb_s2m_tdata(34);
m_cc_wb_s2m_tuser <= m_op_wb_m2s_tdata;
m_cc_wb_s2m.stall <= '0';



end Behavioral;
