-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : tb_pcie3_tlp_cq_to_wb.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.pcie_gen3_pkg.all;
use work.pcie3_mm_pkg.all;


entity tb_pcie3_tlp_cq_to_wb is
--  Port ( );
end tb_pcie3_tlp_cq_to_wb;

architecture Behavioral of tb_pcie3_tlp_cq_to_wb is
component pcie3_tlp_cq_to_wb is
  generic (
    g_tdata_width: natural:= 128
  );
  Port ( clk_i : in STD_LOGIC;
         rst_n_i : in STD_LOGIC;
           
         s_axis_cq_m2s: in  t_axis_cq_m2s;
         s_axis_cq_s2m: out t_axis_cq_s2m; 
         
         m_wb_m2s: out t_wishbone_a32_d32_m2s;
         m_wb_m2s_tuser: out std_logic_vector(2 downto 0);
         m_wb_s2m_tready: in std_logic;
                  
         m_fifo_m2s_tvalid: out std_logic;
         m_fifo_m2s_tdata:  out std_logic_vector(g_tdata_width-1 downto 0);
         m_fifo_s2m_tready: in std_logic
  );
end component pcie3_tlp_cq_to_wb;

component pcie3_tlp_pending_fifo is
  generic (
    g_tdata_width: natural:= 128;
    g_depth: natural:= 1
  );
  port (
    clk_i: in std_logic;
    rst_n_i: in std_logic;
    -- cq header
    s_m2s_tvalid: in std_logic;
    s_m2s_tdata:  in std_logic_vector(g_tdata_width-1 downto 0);
    s_s2m_tready: out std_logic;
    
    m_m2s_tvalid: out std_logic;
    m_m2s_tdata:  out std_logic_vector(g_tdata_width-1 downto 0);
    m_s2m_tready: in std_logic
   );
end component pcie3_tlp_pending_fifo;

constant PERIOD : time := 4ns;

  constant c_pkt1: std_logic_vector(255 downto 0) := x"0000000000000000000000000102030400580000000008010000000000000010"; -- write to port
  constant c_user1: std_logic_vector(84 downto 0) := '0' & x"1fe579fc001000f00000f";
  constant c_pkt2: std_logic_vector(255 downto 0) := x"0000000000000000000000000000000000580001000000010000000000000010"; -- read from port
  constant c_user2: std_logic_vector(84 downto 0) := '1' & x"ffff5dfc001000000000f";
  

signal clk_i : std_logic;
signal rst_n_i : std_logic;


 signal s_axis_cq_m2s:  t_axis_cq_m2s := c_axis_cq_m2s_zero;
 signal s_axis_cq_s2m:  t_axis_cq_s2m; 
         
 signal m_wb_m2s:  t_wishbone_a32_d32_m2s;
 signal m_wb_m2s_tuser:  std_logic_vector(2 downto 0);
 signal m_wb_s2m_tready:  std_logic := '1';

 signal m_fifo_m2s_tvalid:  std_logic;
 signal m_fifo_m2s_tdata:   std_logic_vector(127 downto 0);
 signal m_fifo_s2m_tready:  std_logic := '1';


begin

 clk_stim: process
begin
  clk_i <= '0';
  wait for PERIOD/2;
  clk_i <= '1';
  wait for PERIOD/2;
end process;

rst_stim: process
begin
  rst_n_i <= '0';
  wait for PERIOD*4;
  rst_n_i <= '1';
  wait;
end process;


uut: pcie3_tlp_cq_to_wb 
  generic map (
    g_tdata_width => 128
  )
  Port map (
         clk_i  => clk_i,
         rst_n_i => rst_n_i,
           
         s_axis_cq_m2s => s_axis_cq_m2s,
         s_axis_cq_s2m => s_axis_cq_s2m, 
         
         m_wb_m2s => m_wb_m2s,
         m_wb_m2s_tuser => m_wb_m2s_tuser,
         m_wb_s2m_tready => m_wb_s2m_tready,
                  
         m_fifo_m2s_tvalid => m_fifo_m2s_tvalid,
         m_fifo_m2s_tdata => m_fifo_m2s_tdata,
         m_fifo_s2m_tready => m_fifo_s2m_tready
  );

uut_cycle: pcie3_tlp_pending_fifo
  generic map (
    g_tdata_width => 3,
    g_depth => 1
  )
  port map (
         clk_i  => clk_i,
         rst_n_i => rst_n_i,
    -- cq header
    s_m2s_tvalid => m_wb_m2s.stb,
    s_m2s_tdata => m_wb_m2s_tuser,
    s_s2m_tready => m_wb_s2m_tready,
    
    m_m2s_tvalid => open,
    m_m2s_tdata => open,
    m_s2m_tready => '1'
   );

uut_pending: pcie3_tlp_pending_fifo
  generic map (
    g_tdata_width => 128,
    g_depth => 1
  )
  port map (
         clk_i  => clk_i,
         rst_n_i => rst_n_i,
    -- cq header
    s_m2s_tvalid => m_fifo_m2s_tvalid,
    s_m2s_tdata => m_fifo_m2s_tdata,
    s_s2m_tready => m_fifo_s2m_tready,
    
    m_m2s_tvalid => open,
    m_m2s_tdata => open,
    m_s2m_tready => '1'
   );
  
  
p_stim: process
begin
  wait for PERIOD*10;
  s_axis_cq_m2s.tdata <= c_pkt1;
  s_axis_cq_m2s.tuser <= c_user1;
  s_axis_cq_m2s.tvalid <= '1';
  wait;
end process;  

end Behavioral;
