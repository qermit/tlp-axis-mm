-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : tb_pcie3_tlp_fifo.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.pcie3_mm_components_pkg.all;


entity tb_pcie3_tlp_fifo is
--  Port ( );
end tb_pcie3_tlp_fifo;

architecture Behavioral of tb_pcie3_tlp_fifo is
  constant c_fifo_width: natural := 32;
  signal     s_m2s_tvalid: std_logic := '0';
  signal    s_m2s_tdata: std_logic_vector(c_fifo_width-1 downto 0) := (others => '0');
  signal    s_s2m_tready: std_logic := '0';
    
  signal    m_m2s_tvalid: std_logic := '0';
  signal    m_m2s_tdata: std_logic_vector(c_fifo_width-1 downto 0) := (others => '0');
  signal    m_s2m_tready: std_logic := '0';

  constant PERIOD : time := 4ns;
  signal clk_i : std_logic;
  signal rst_n_i : std_logic;
begin

clk_stim: process
begin
  clk_i <= '0';
  wait for PERIOD/2;
  clk_i <= '1';
  wait for PERIOD/2;
end process;

rst_stim: process
begin
  rst_n_i <= '0';
  wait for PERIOD*4;
  rst_n_i <= '1';
  wait;
end process;

p_stim: process
begin
 wait for PERIOD*100;
 m_s2m_tready <= '1';
 wait;
end process;

u_wishbone_fifo: pcie3_tlp_fifo 
  generic map (
    g_tdata_width => c_fifo_width,
    g_depth => 2,
    g_async => false
  )  
  port map (
    clk_i => clk_i,
    rst_n_i => rst_n_i,
    
    clk2_i => clk_i,
    rst2_n_i => rst_n_i,
    
    
    -- cq header
    s_m2s_tvalid => s_m2s_tvalid,
    s_m2s_tdata  => s_m2s_tdata,
    s_s2m_tready => s_s2m_tready,
    
    m_m2s_tvalid => m_m2s_tvalid,
    m_m2s_tdata  => m_m2s_tdata,
    m_s2m_tready => m_s2m_tready
   );
   
p1: process(   clk_i)
begin
  if rising_edge(clk_i) then
    if s_m2s_tvalid = '1' and s_s2m_tready = '1' then
      s_m2s_tdata <= std_logic_vector(unsigned(s_m2s_tdata) + 1);
    end if;
    s_m2s_tvalid <= '1';
    
    if rst_n_i = '0' then
      s_m2s_tdata  <= x"ffff0000";
      s_m2s_tvalid <= '0';
    end if;
  end if;
end process;
   
end Behavioral;
