-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : tb_pcie3_tlp_to_wb.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.pcie_gen3_pkg.all;
use work.pcie3_mm_pkg.all;
use work.pcie3_mm_components_pkg.all;


entity tb_pcie3_tlp_to_wb is
--  Port ( );
end tb_pcie3_tlp_to_wb;

architecture Behavioral of  tb_pcie3_tlp_to_wb is

component wb_dummy_slave is
    Port ( clk_i : in STD_LOGIC;
    rst_n_i : in std_logic;
               -- Wishbone master interface
           m_wb_m2s: in t_wishbone_a32_d32_m2s;
           m_wb_s2m: out t_wishbone_a32_d32_s2m
    );
end component wb_dummy_slave;


constant PERIOD : time := 4ns;

--  constant c_pkt1: std_logic_vector(255 downto 0) := x"0000000000000000000000000102030400580000000008010000000000000010"; -- write to port
--  constant c_user1: std_logic_vector(84 downto 0) := '0' & x"1fe579fc001000f00000f";
--  constant c_pkt2: std_logic_vector(255 downto 0) := x"0000000000000000000000000000000000580001000000010000000000000010"; -- read from port
--  constant c_user2: std_logic_vector(84 downto 0) := '1' & x"ffff5dfc001000000000f";  
--  constant c_expected_cc_tdata: std_logic_vector(255 downto 0) := x"000000000000000000000000000000000102030400aaf8010000000100040010";
  
  constant c_pkt1: std_logic_vector(255 downto 0) := x"00000000090a0b0c050607080102030400580000000008030000000000000010"; -- write to port
  constant c_user1: std_logic_vector(84 downto 0) := '0' & x"1fe579fc0010fff0000ff";

--  constant c_pkt1: std_logic_vector(255 downto 0) := x"0000000000000000050607080102030400580000000008020000000000000010"; -- write to port
--  constant c_user1: std_logic_vector(84 downto 0) := '0' & x"1fe579fc00100ff0000ff";
  constant c_pkt2: std_logic_vector(255 downto 0) := x"0000000000000000000000000000000000580001000000030000000000000010"; -- read from port
  constant c_user2: std_logic_vector(84 downto 0) := '1' & x"ffff5dfc001000000000f";  


signal r_data_index: natural;
constant c_data_to_send : a_axis_cq_m2s:= (
  0 => (tdata => x"0000000000000000000000000102030400580000000008010000000000000010",
   tlast => '1',
   tkeep => x"1f",
   tuser => '0' & x"ffff5dfc001000000000f",
   tvalid => '1'),
  1 => (tdata => x"0000000000000000000000000000000000580001000000010000000000000010",
   tlast => '1',
   tkeep => x"0f",
   tuser => '1' & x"ffff5dfc001000000000f",
   tvalid => '1')
);

signal clk_i : std_logic;
signal rst_n_i : std_logic;


 signal s_axis_cq_m2s:  t_axis_cq_m2s := c_axis_cq_m2s_zero;
 signal s_axis_cq_s2m:  t_axis_cq_s2m; 

 signal m_axis_cc_m2s:  t_axis_cc_m2s := c_axis_cc_m2s_zero;
 signal m_axis_cc_s2m:  t_axis_cc_s2m := c_axis_cc_s2m_zero;

         
 signal m_wb_m2s:  t_wishbone_a32_d32_m2s;
 signal m_wb_s2m:  t_wishbone_a32_d32_s2m :=  (  
    ack => '0', 
    err  => '0',
    rty  => '0',
    stall  => '0',
    dat => x"00000000");




begin

 clk_stim: process
begin
  clk_i <= '0';
  wait for PERIOD/2;
  clk_i <= '1';
  wait for PERIOD/2;
end process;

rst_stim: process
begin
  rst_n_i <= '0';
  wait for PERIOD*4;
  rst_n_i <= '1';
  wait;
end process;


m_axis_cc_s2m.tready <= '1';
 
uut: pcie3_tlp_to_wb 
  generic map (
    g_asynchronous => true
  )
  Port map (
         clk_i  => clk_i,
         rst_n_i => rst_n_i,
         clk2_i => clk_i,
         rst2_n_i => rst_n_i,
           
         s_axis_cq_m2s => s_axis_cq_m2s,
         s_axis_cq_s2m => s_axis_cq_s2m, 
         
         m_axis_cc_m2s => m_axis_cc_m2s,
         m_axis_cc_s2m => m_axis_cc_s2m,
                             
           -- Wishbone master interface
         m_wb_m2s => m_wb_m2s,
         m_wb_s2m => m_wb_s2m
  );

  u_dummy: wb_dummy_slave 
    Port map (clk_i  => clk_i,
         rst_n_i => rst_n_i,
               -- Wishbone master interface
           m_wb_m2s => m_wb_m2s,
         m_wb_s2m => m_wb_s2m
    );
  
--p_stim: process
--begin
--  wait for PERIOD*10;
--  s_axis_cq_m2s.tdata <= c_pkt1;
--  s_axis_cq_m2s.tuser <= c_user1;
--  s_axis_cq_m2s.tvalid <= '1';
--  wait for PERIOD;
--  s_axis_cq_m2s.tdata <= c_pkt2;
--  s_axis_cq_m2s.tuser <= c_user2;
--  wait for PERIOD*5;
--  s_axis_cq_m2s.tvalid <= '0';
  
----  s_axis_cq_m2s <= c_axis_cq_m2s_zero;
--  wait;
--end process;  


p1: process(clk_i)
begin
  if rising_edge(clk_i) then
    if (s_axis_cq_m2s.tvalid = '1' and s_axis_cq_s2m.tready ='1') or (s_axis_cq_m2s.tvalid='0')  then
      if r_data_index < c_data_to_send'length then
        s_axis_cq_m2s <= c_data_to_send(r_data_index);
        r_data_index <= r_data_index + 1;
      else
        s_axis_cq_m2s <= c_axis_cq_m2s_zero;
      end if;
    end if;
    
    if rst_n_i = '0' then
      r_data_index <= 0;
      s_axis_cq_m2s <= c_axis_cq_m2s_zero;
    end if;
  end if;
end process;

end Behavioral;
