-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : tb_pcie3_tlp_wb_to_cc.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.pcie_gen3_pkg.all;
use work.pcie3_mm_pkg.all;
use work.pcie3_mm_components_pkg.all;


entity tb_pcie3_tlp_wb_to_cc is
--  Port ( );
end tb_pcie3_tlp_wb_to_cc;

architecture Behavioral of tb_pcie3_tlp_wb_to_cc is

constant PERIOD : time := 4ns;

  constant c_pkt1: std_logic_vector(255 downto 0) := x"0000000000000000000000000102030400580000000008010000000000000010"; -- write to port
  constant c_user1: std_logic_vector(84 downto 0) := '0' & x"1fe579fc001000f00000f";
  constant c_pkt2: std_logic_vector(255 downto 0) := x"0000000000000000000000000000000000580001000000010000000000000010"; -- read from port
  constant c_user2: std_logic_vector(84 downto 0) := '1' & x"ffff5dfc001000000000f";
  

signal clk_i : std_logic;
signal rst_n_i : std_logic;


 signal m_axis_cc_m2s:  t_axis_cc_m2s := c_axis_cc_m2s_zero;
 signal m_axis_cc_s2m:  t_axis_cc_s2m := c_axis_cc_s2m_zero;

         
 signal m_wb_s2m:  t_wishbone_a32_d32_s2m :=  (  
    ack => '0', 
    err  => '0',
    rty  => '0',
    stall  => '0',
    dat => x"00000000");
    
    
    constant c_pending_fifo_width: natural := 128;
    
    
signal m_compl_wb_s2m: t_wishbone_a32_d32_s2m := (stall => '0', ack=>'1', rty=>'1', err => '1', dat => x"deadbeef");
signal m_compl_wb_s2m_tuser: std_logic_vector(2 downto 0) := "011"; 
signal m_compl_wb_s2m_tvalid: std_logic := '1'; 
signal m_compl_wb_m2s_tready: std_logic;

signal s_fifo_m2s_tvalid:  std_logic:='1';
signal s_fifo_m2s_tdata:   std_logic_vector(c_pending_fifo_width-1 downto 0) := (others =>'0');
signal s_fifo_s2m_tready:  std_logic;
begin
     clk_stim: process
begin
  clk_i <= '0';
  wait for PERIOD/2;
  clk_i <= '1';
  wait for PERIOD/2;
end process;

rst_stim: process
begin
  rst_n_i <= '0';
  wait for PERIOD*4;
  rst_n_i <= '1';
  wait;
end process;

rst_stim_recv: process
begin
  m_axis_cc_s2m.tready <= '0';
  wait for PERIOD*20;
  m_axis_cc_s2m.tready <= '1';
  wait for PERIOD*20;
  m_axis_cc_s2m.tready <= '0';
  wait for PERIOD*20;
  m_axis_cc_s2m.tready <= '1';
  wait for PERIOD*20;
  wait;
end process;

-- completer
u_completer_compl : pcie3_tlp_wb_to_cc
  generic map (
     g_tdata_width => c_pending_fifo_width
  )
  Port map (
    clk_i => clk_i,
    rst_n_i => rst_n_i,
           
         s_axis_cc_m2s => m_axis_cc_m2s,
         s_axis_cc_s2m => m_axis_cc_s2m,
          
         -- wishbone return fifo
         s_wb_s2m => m_compl_wb_s2m,
         s_wb_s2m_tuser => m_compl_wb_s2m_tuser,
         s_wb_s2m_tvalid => m_compl_wb_s2m_tvalid,
         s_wb_m2s_tready => m_compl_wb_m2s_tready,
         
        -- pending requests fifo       
         s_fifo_m2s_tvalid => s_fifo_m2s_tvalid,
         s_fifo_m2s_tdata => s_fifo_m2s_tdata,
         s_fifo_s2m_tready => s_fifo_s2m_tready
   );


p: process(clk_i)
begin
  if rising_edge(clk_i) then
    m_compl_wb_s2m_tuser(0) <= not m_compl_wb_s2m_tuser(0);
    m_compl_wb_s2m_tuser(1) <= not m_compl_wb_s2m_tuser(1);
    m_compl_wb_s2m.dat <= std_logic_vector(unsigned(m_compl_wb_s2m.dat) + 1);
  end if;
end process;
end Behavioral;
