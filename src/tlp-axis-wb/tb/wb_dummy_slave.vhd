-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : wb_dummy_slave.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2020 Piotr Miedzik 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.pcie3_mm_pkg.all;


entity wb_dummy_slave is
    Port ( clk_i : in STD_LOGIC;
    rst_n_i : in std_logic;
               -- Wishbone master interface
           m_wb_m2s: in t_wishbone_a32_d32_m2s;
           m_wb_s2m: out t_wishbone_a32_d32_s2m
    );
end wb_dummy_slave;

architecture Behavioral of wb_dummy_slave is
 signal r_reg : std_logic_vector(31 downto 0);
begin

m_wb_s2m.stall <= '0';
m_wb_s2m.err   <= '0';
m_wb_s2m.rty   <= '0';
m_wb_s2m.dat   <= r_reg;


p_write: process(clk_i)
begin
  if rising_edge(clk_i) then
    if m_wb_m2s.stb = '1' and m_wb_m2s.cyc = '1' and m_wb_m2s.we = '1' then
      r_reg <=   m_wb_m2s.dat;
    end if;
    
    if rst_n_i = '0' then
      r_reg <= (others => '0');
    end if;
  end if;
end process;


p1: process(clk_i)
begin
  if rising_edge(clk_i) then
    if m_wb_m2s.stb = '1' and m_wb_m2s.cyc = '1' then
      m_wb_s2m.ack <= '1';
      
    else
      m_wb_s2m.ack <= '0';    
    end if;
    
    if rst_n_i = '0' then
      m_wb_s2m.ack <= '0';
    end if;
  end if;
end process;

end Behavioral;
