-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : axis_interconnect_cc.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.pcie_gen3_pkg.all;


entity axis_interconnect_cc is
  generic (
     g_num_masters: natural:=4
  );
  Port ( clk_i : in STD_LOGIC;
         rst_n_i : in STD_LOGIC;
           
         m_axis_cc_m2s: out t_axis_cc_m2s;
         m_axis_cc_s2m: in  t_axis_cc_s2m;
           
         s_axis_cc_m2s: in  a_axis_cc_m2s(g_num_masters-1 downto 0);
         s_axis_cc_s2m: out a_axis_cc_s2m(g_num_masters-1 downto 0)
);
end axis_interconnect_cc;

architecture Behavioral of axis_interconnect_cc is

COMPONENT axis_interconnect_cc_ip
  PORT (
    aclk : IN STD_LOGIC;
    aresetn : IN STD_LOGIC;
    s_axis_tvalid : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axis_tready : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axis_tdata : IN STD_LOGIC_VECTOR(1023 DOWNTO 0);
    s_axis_tkeep : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    s_axis_tlast : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axis_tuser : IN STD_LOGIC_VECTOR(131 DOWNTO 0);
    m_axis_tvalid : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axis_tready : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axis_tdata : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    m_axis_tkeep : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    m_axis_tlast : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axis_tuser : OUT STD_LOGIC_VECTOR(32 DOWNTO 0);
    s_req_suppress : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_decode_err : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
  );
END COMPONENT;

signal    s_axis_tready :  STD_LOGIC_VECTOR(3 DOWNTO 0);
signal    s_axis_tvalid :  STD_LOGIC_VECTOR(3 DOWNTO 0);
signal    s_axis_tdata :  STD_LOGIC_VECTOR(1023 DOWNTO 0);
signal    s_axis_tlast :  STD_LOGIC_VECTOR(3 DOWNTO 0);
 signal   s_axis_tuser : STD_LOGIC_VECTOR(131 DOWNTO 0);
signal    s_axis_tkeep : STD_LOGIC_VECTOR(127 DOWNTO 0);


signal   s_req_suppress :  STD_LOGIC_VECTOR(3 DOWNTO 0):=(others => '0');
signal   m_axis_tkeep :  STD_LOGIC_VECTOR(31 DOWNTO 0);

begin

GEN_MASTER: for i in 0 to g_num_masters-1 generate
begin
  s_axis_tvalid(i) <= s_axis_cc_m2s(i).tvalid;  
  s_axis_cc_s2m(i).tready <= s_axis_tready(i);  
  s_axis_tdata(256*i+255 downto 256*i+0) <= s_axis_cc_m2s(i).tdata;
  
  s_axis_tkeep(32*i+3  downto 32*i+0)  <= (others => s_axis_cc_m2s(i).tkeep(0));
  s_axis_tkeep(32*i+7  downto 32*i+4)  <= (others => s_axis_cc_m2s(i).tkeep(1));
  s_axis_tkeep(32*i+11 downto 32*i+8)  <= (others => s_axis_cc_m2s(i).tkeep(2));
  s_axis_tkeep(32*i+15 downto 32*i+12) <= (others => s_axis_cc_m2s(i).tkeep(3));
  s_axis_tkeep(32*i+19 downto 32*i+16) <= (others => s_axis_cc_m2s(i).tkeep(4));
  s_axis_tkeep(32*i+23 downto 32*i+20) <= (others => s_axis_cc_m2s(i).tkeep(5));
  s_axis_tkeep(32*i+27 downto 32*i+24) <= (others => s_axis_cc_m2s(i).tkeep(6));
  s_axis_tkeep(32*i+31 downto 32*i+28) <= (others => s_axis_cc_m2s(i).tkeep(7));
  


  s_axis_tlast(i) <= s_axis_cc_m2s(i).tlast; 
  s_axis_tuser(33*i+32 downto 33*i+0) <= s_axis_cc_m2s(i).tuser;

end generate;

u_switch : axis_interconnect_cc_ip
  PORT MAP (
    aclk => clk_i,
    aresetn => rst_n_i,
    
    s_axis_tvalid => s_axis_tvalid,
    s_axis_tready => s_axis_tready,
    s_axis_tdata => s_axis_tdata,
    s_axis_tkeep => s_axis_tkeep,
    s_axis_tlast => s_axis_tlast,
    s_axis_tuser => s_axis_tuser,
    
    m_axis_tvalid(0) => m_axis_cc_m2s.tvalid,
    m_axis_tready(0) => m_axis_cc_s2m.tready,
    m_axis_tdata => m_axis_cc_m2s.tdata,
    m_axis_tkeep => m_axis_tkeep,
    m_axis_tlast(0) => m_axis_cc_m2s.tlast,
    m_axis_tuser => m_axis_cc_m2s.tuser,
    
    s_req_suppress => s_req_suppress,
    s_decode_err => open
  );
  
  m_axis_cc_m2s.tkeep(0) <= m_axis_tkeep(0);
  m_axis_cc_m2s.tkeep(1) <= m_axis_tkeep(4);
  m_axis_cc_m2s.tkeep(2) <= m_axis_tkeep(8);
  m_axis_cc_m2s.tkeep(3) <= m_axis_tkeep(12);
  m_axis_cc_m2s.tkeep(4) <= m_axis_tkeep(16);
  m_axis_cc_m2s.tkeep(5) <= m_axis_tkeep(20);
  m_axis_cc_m2s.tkeep(6) <= m_axis_tkeep(24);
  m_axis_cc_m2s.tkeep(7) <= m_axis_tkeep(28);  

end Behavioral;
