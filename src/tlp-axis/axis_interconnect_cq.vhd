-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : axis_interconnect_cq.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.pcie_gen3_pkg.all;


entity axis_interconnect_cq is
   generic (
     g_num_masters: natural:=4
   );
   Port ( clk_i : in STD_LOGIC;
           rst_n_i : in STD_LOGIC;
           
         s_axis_cq_m2s: in  t_axis_cq_m2s;
         s_axis_cq_tdest: in std_logic_vector(1 downto 0);
         s_axis_cq_s2m: out t_axis_cq_s2m;          
           
         m_axis_cq_m2s: out a_axis_cq_m2s(g_num_masters-1 downto 0);
         m_axis_cq_s2m: in  a_axis_cq_s2m(g_num_masters-1 downto 0)
   );
end axis_interconnect_cq;

architecture Behavioral of axis_interconnect_cq is
COMPONENT axis_interconnect_cq_ip
  PORT (
    aclk : IN STD_LOGIC;
    aresetn : IN STD_LOGIC;
    s_axis_tvalid : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    s_axis_tready : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    s_axis_tdata : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    s_axis_tkeep : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axis_tlast : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    s_axis_tdest : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axis_tuser : IN STD_LOGIC_VECTOR(84 DOWNTO 0);
    m_axis_tvalid : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axis_tready : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axis_tdata : OUT STD_LOGIC_VECTOR(1023 DOWNTO 0);
    m_axis_tkeep : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    m_axis_tlast : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axis_tdest : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axis_tuser : OUT STD_LOGIC_VECTOR(339 DOWNTO 0);
    s_decode_err : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
  );
END COMPONENT;

signal    s_axis_tkeep :  STD_LOGIC_VECTOR(31 DOWNTO 0);

signal s_axis_tvalid: std_logic_vector(0 downto 0);
signal m_axis_tvalid : STD_LOGIC_VECTOR(3 DOWNTO 0);
signal m_axis_tready : STD_LOGIC_VECTOR(3 DOWNTO 0);
signal m_axis_tdata : STD_LOGIC_VECTOR(1023 DOWNTO 0);
signal m_axis_tkeep : STD_LOGIC_VECTOR(127 DOWNTO 0);
signal m_axis_tlast : STD_LOGIC_VECTOR(3 DOWNTO 0);
signal m_axis_tuser : STD_LOGIC_VECTOR(339 DOWNTO 0);
begin
s_axis_tvalid(0) <= s_axis_cq_m2s.tvalid;


s_axis_tkeep(3 downto 0) <= (others => s_axis_cq_m2s.tkeep(0));
s_axis_tkeep(7 downto 4) <= (others => s_axis_cq_m2s.tkeep(1));
s_axis_tkeep(11 downto 8) <= (others => s_axis_cq_m2s.tkeep(2));
s_axis_tkeep(15 downto 12) <= (others => s_axis_cq_m2s.tkeep(3));
s_axis_tkeep(19 downto 16) <= (others => s_axis_cq_m2s.tkeep(4));
s_axis_tkeep(23 downto 20) <= (others => s_axis_cq_m2s.tkeep(5));
s_axis_tkeep(27 downto 24) <= (others => s_axis_cq_m2s.tkeep(6));
s_axis_tkeep(31 downto 28) <= (others => s_axis_cq_m2s.tkeep(7));

u_switch : axis_interconnect_cq_ip
  PORT MAP (
    aclk => clk_i,
    aresetn => rst_n_i,
    s_axis_tvalid => s_axis_tvalid,
    s_axis_tready(0) => s_axis_cq_s2m.tready,
    s_axis_tdata => s_axis_cq_m2s.tdata,
    s_axis_tkeep => s_axis_tkeep,
    s_axis_tlast(0) => s_axis_cq_m2s.tlast ,
    s_axis_tdest => s_axis_cq_tdest,
    s_axis_tuser => s_axis_cq_m2s.tuser,
    
    m_axis_tvalid => m_axis_tvalid,
    m_axis_tready => m_axis_tready,
    m_axis_tdata => m_axis_tdata,
    m_axis_tkeep => m_axis_tkeep,
    m_axis_tlast => m_axis_tlast,
    m_axis_tdest => open,
    m_axis_tuser => m_axis_tuser,
    s_decode_err => open
  );
  
GEN_MASTER: for i in 0 to g_num_masters-1 generate
begin
  m_axis_tready(i) <= m_axis_cq_s2m(i).tready;
  m_axis_cq_m2s(i).tvalid <= m_axis_tvalid(i);
  m_axis_cq_m2s(i).tdata <= m_axis_tdata(256*i+255 downto 256*i+0);
  
  m_axis_cq_m2s(i).tkeep(0) <= m_axis_tkeep(32*i+0);
  m_axis_cq_m2s(i).tkeep(1) <= m_axis_tkeep(32*i+4);
  m_axis_cq_m2s(i).tkeep(2) <= m_axis_tkeep(32*i+8);
  m_axis_cq_m2s(i).tkeep(3) <= m_axis_tkeep(32*i+12);
  m_axis_cq_m2s(i).tkeep(4) <= m_axis_tkeep(32*i+16);
  m_axis_cq_m2s(i).tkeep(5) <= m_axis_tkeep(32*i+20);
  m_axis_cq_m2s(i).tkeep(6) <= m_axis_tkeep(32*i+24);
  m_axis_cq_m2s(i).tkeep(7) <= m_axis_tkeep(32*i+28);

  m_axis_cq_m2s(i).tlast <= m_axis_tlast(i);
  m_axis_cq_m2s(i).tuser <= m_axis_tuser(85*i+84 downto 85*i+0);

end generate;
  
end Behavioral;
