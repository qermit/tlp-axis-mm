#------------------------------------------------------------------------------
#- Title      :
#- Project    : TLP-AXIS MM
#------------------------------------------------------------------------------
#- File       : ip_core_create.tcl
#- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
#- Company    :
#- Created    : 2020-09-13
#- Last update: 2020-09-13
#- License    : This is a PUBLIC DOMAIN code, published under
#-              Creative Commons CC0 license
#- Platform   :
#- Standard   : VHDL'93/02
#------------------------------------------------------------------------------
#- Description: 
#------------------------------------------------------------------------------
#- Copyleft (ↄ) 2017 
#------------------------------------------------------------------------------
#- Revisions  : see git commit log
#------------------------------------------------------------------------------
#- External requirements :
#------------------------------------------------------------------------------
#- Implementation details
#------------------------------------------------------------------------------
#-
#------------------------------------------------------------------------------

create_ip -name axis_switch -vendor xilinx.com -library ip -version 1.1 -module_name axis_interconnect_cq_ip
# one slave interface, four master interfaces
set_property -dict [list CONFIG.NUM_SI {1} CONFIG.NUM_MI {4}] [get_ips axis_interconnect_cq_ip]
# routing
set_property -dict [list CONFIG.M00_AXIS_BASETDEST {0x00000003} CONFIG.M01_AXIS_BASETDEST {0x00000000} CONFIG.M02_AXIS_BASETDEST {0x00000001} CONFIG.M03_AXIS_BASETDEST {0x00000002} CONFIG.M00_AXIS_HIGHTDEST {0x00000003} CONFIG.M01_AXIS_HIGHTDEST {0x00000000} CONFIG.M02_AXIS_HIGHTDEST {0x00000001} CONFIG.M03_AXIS_HIGHTDEST {0x00000002}]  [get_ips axis_interconnect_cq_ip]
# interface widht and fields
set_property -dict [list CONFIG.HAS_TKEEP	true CONFIG.HAS_TLAST	true CONFIG.HAS_TSTRB	false ]  [get_ips axis_interconnect_cq_ip]
set_property -dict [list CONFIG.TDATA_NUM_BYTES	32 CONFIG.TDEST_WIDTH	2 CONFIG.TID_WIDTH	0 CONFIG.TUSER_WIDTH	85]  [get_ips axis_interconnect_cq_ip]


create_ip -name axis_switch -vendor xilinx.com -library ip -version 1.1 -module_name axis_interconnect_cc_ip
# four slave interfaces, one master interface
set_property -dict [list CONFIG.NUM_SI {4} CONFIG.NUM_MI {1}] [get_ips axis_interconnect_cc_ip]
# interface width and fields
set_property -dict [list CONFIG.HAS_TKEEP	true CONFIG.HAS_TLAST	true CONFIG.HAS_TSTRB	false ]  [get_ips axis_interconnect_cc_ip]
set_property -dict [list CONFIG.TDATA_NUM_BYTES	32 CONFIG.TDEST_WIDTH	0 CONFIG.TID_WIDTH	0 CONFIG.TUSER_WIDTH	33]  [get_ips axis_interconnect_cc_ip]
# arbitration params
set_property -dict [list CONFIG.ARB_ON_TLAST {1} CONFIG.ARB_ALGORITHM {0} CONFIG.ARB_ON_MAX_XFERS {0}] [get_ips axis_interconnect_cc_ip]

