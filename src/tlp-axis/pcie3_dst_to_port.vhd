-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : pcie3_dst_to_port.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.pcie_gen3_pkg.all;


entity pcie3_dst_to_port is
  generic (
    g_fallback: integer:= -1;
    g_routing: a_pcie_gen3_routing(2 downto 0) := (others => c_pcie_gen3_routing_record_disabled)
  );
  Port (
    tdata : in STD_LOGIC_VECTOR (255 downto 0);
    oport : out STD_LOGIC_VECTOR (1 downto 0)
  );
end pcie3_dst_to_port;

architecture Behavioral of pcie3_dst_to_port is

    function f_fallback_port(p_fallback:integer; p_length: natural) 
        return std_logic_vector is
      variable v_fallback:std_logic_vector(p_length-1 downto 0):= (others => '1');
    begin
        if p_fallback >= 0 then
           v_fallback:= std_logic_vector(to_unsigned(p_fallback, p_length));
        end if;
        return v_fallback;
    end function;

  constant c_fallback_port: std_logic_vector := f_fallback_port(g_fallback, oport'length);
  signal s_cq_header: t_axis_cq_header;

begin
s_cq_header <= pcie_cq_header_to_type(tdata(127 downto 0));

oport <= "00" when g_routing(0).enabled = true and (s_cq_header.req_type = pcie_req_mr or s_cq_header.req_type = pcie_req_mw) and ((s_cq_header.bar_id and g_routing(0).routing_mask) = g_routing(0).routing) and ((s_cq_header.target_function and g_routing(0).target_function_mask) = g_routing(0).target_function) else
         "01" when g_routing(1).enabled = true and (s_cq_header.req_type = pcie_req_mr or s_cq_header.req_type = pcie_req_mw) and ((s_cq_header.bar_id and g_routing(1).routing_mask) = g_routing(1).routing) and ((s_cq_header.target_function and g_routing(1).target_function_mask) = g_routing(1).target_function) else
         "10" when g_routing(2).enabled = true and (s_cq_header.req_type = pcie_req_mr or s_cq_header.req_type = pcie_req_mw) and ((s_cq_header.bar_id and g_routing(2).routing_mask) = g_routing(2).routing) and ((s_cq_header.target_function and g_routing(2).target_function_mask) = g_routing(2).target_function) else
         c_fallback_port;

end Behavioral;
