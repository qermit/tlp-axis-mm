-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : pcie3_tlp_interconnect.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.pcie_gen3_pkg.all;


entity pcie3_tlp_interconnect is
  generic (
      g_fallback: integer:= -1;
      g_routing: a_pcie_gen3_routing(2 downto 0) := (others => c_pcie_gen3_routing_record_disabled)
  );
  Port ( clk_i : in STD_LOGIC;
         rst_n_i : in STD_LOGIC;
           
         s_axis_cq_m2s: in  t_axis_cq_m2s;
         s_axis_cq_s2m: out t_axis_cq_s2m;           
         m_axis_cc_m2s: out t_axis_cc_m2s;
         m_axis_cc_s2m: in  t_axis_cc_s2m;
           
         m_axis_cq_m2s: out a_axis_cq_m2s(2 downto 0);
         m_axis_cq_s2m: in  a_axis_cq_s2m(2 downto 0);
         s_axis_cc_m2s: in  a_axis_cc_m2s(2 downto 0);
         s_axis_cc_s2m: out a_axis_cc_s2m(2 downto 0)
);
end pcie3_tlp_interconnect;

architecture Behavioral of pcie3_tlp_interconnect is

COMPONENT pcie_cq_dst_decoder is
    generic (
      g_fallback: integer:= -1;
      g_routing: a_pcie_gen3_routing(2 downto 0) := (others => c_pcie_gen3_routing_record_disabled)
    );
    Port ( clk_i : in STD_LOGIC;
           rst_n_i : in STD_LOGIC;
           
           s_axis_cq_m2s: in t_axis_cq_m2s;
           s_axis_cq_s2m: out t_axis_cq_s2m;
           
           m_tdest : out std_logic_vector(1 downto 0);
           m_axis_cq_m2s: out t_axis_cq_m2s;
           m_axis_cq_s2m: in t_axis_cq_s2m
   );
end COMPONENT pcie_cq_dst_decoder;

COMPONENT pcie3_unsupported_completer is
    Port ( clk_i : in STD_LOGIC;
           rst_n_i : in STD_LOGIC;
           
           s_axis_cq_m2s: in t_axis_cq_m2s;
           s_axis_cq_s2m: out t_axis_cq_s2m;
           
           m_axis_cc_m2s: out t_axis_cc_m2s;
           m_axis_cc_s2m: in t_axis_cc_s2m
           );
end COMPONENT pcie3_unsupported_completer;

component axis_interconnect_cq is
   generic (
     g_num_masters: natural:=4
   );
   Port ( clk_i : in STD_LOGIC;
           rst_n_i : in STD_LOGIC;
           
         s_axis_cq_m2s: in  t_axis_cq_m2s;
         s_axis_cq_tdest: in std_logic_vector(1 downto 0);
         s_axis_cq_s2m: out t_axis_cq_s2m;          
           
         m_axis_cq_m2s: out a_axis_cq_m2s(g_num_masters-1 downto 0);
         m_axis_cq_s2m: in  a_axis_cq_s2m(g_num_masters-1 downto 0)
   );
end component axis_interconnect_cq;

COMPONENT axis_interconnect_cc is
  generic (
     g_num_masters: natural:=4
  );
  Port ( clk_i : in STD_LOGIC;
         rst_n_i : in STD_LOGIC;
           
         m_axis_cc_m2s: out t_axis_cc_m2s;
         m_axis_cc_s2m: in  t_axis_cc_s2m;
           
         s_axis_cc_m2s: in  a_axis_cc_m2s(g_num_masters-1 downto 0);
         s_axis_cc_s2m: out a_axis_cc_s2m(g_num_masters-1 downto 0)
);
end COMPONENT axis_interconnect_cc;


  constant c_dest_width: positive:=2;
  
  signal s_decode_err: std_logic;  
  signal s_dest_port: std_logic_vector(c_dest_width - 1 downto 0);
  
  signal v_axis_cq_m2s: t_axis_cq_m2s;
  signal v_axis_cq_s2m: t_axis_cq_s2m;
  
  -- first port is always assigned to unsupported
  signal sig_axis_cq_m2s:  a_axis_cq_m2s(3 downto 0);
  signal sig_axis_cq_s2m:  a_axis_cq_s2m(3 downto 0);           
  signal sig_axis_cc_m2s:  a_axis_cc_m2s(3 downto 0);
  signal sig_axis_cc_s2m:  a_axis_cc_s2m(3 downto 0);
           
begin


u_dst_decoder_cq: pcie_cq_dst_decoder 
 generic map (
   g_fallback => g_fallback,
   g_routing => g_routing
 )
    Port map ( clk_i => clk_i,
           rst_n_i => rst_n_i,

           s_axis_cq_m2s => s_axis_cq_m2s,
           s_axis_cq_s2m => s_axis_cq_s2m,
           
           m_axis_cq_m2s => v_axis_cq_m2s,
           m_axis_cq_s2m => v_axis_cq_s2m,
           m_tdest => s_dest_port
           );

u_tlp_interconnect: axis_interconnect_cq
    Port map( clk_i => clk_i,
           rst_n_i => rst_n_i,
           
           s_axis_cq_m2s => v_axis_cq_m2s,
           s_axis_cq_tdest => s_dest_port,
           s_axis_cq_s2m => v_axis_cq_s2m,
           
           
           m_axis_cq_m2s => sig_axis_cq_m2s,
           m_axis_cq_s2m => sig_axis_cq_s2m
);


GEN_REG:
   for I in 0 to m_axis_cq_m2s'length - 1 generate
     -- todo: isolate when port does not have any routing
     m_axis_cq_m2s(i) <= sig_axis_cq_m2s(i+1);
     sig_axis_cq_s2m(i+1) <= m_axis_cq_s2m(i);
     
     -- connect slave interfaces to interconnect cq
     sig_axis_cc_m2s(i+1) <= s_axis_cc_m2s(i);
     s_axis_cc_s2m(i) <= sig_axis_cc_s2m(i+1);
   end generate GEN_REG;

-- todo: remove when g_fallback /= -1
u_unsupported_cq: pcie3_unsupported_completer 
    Port map ( clk_i => clk_i,
           rst_n_i => rst_n_i,
           
           s_axis_cq_m2s => sig_axis_cq_m2s(0), 
           s_axis_cq_s2m => sig_axis_cq_s2m(0),
           
           m_axis_cc_m2s => sig_axis_cc_m2s(0),
           m_axis_cc_s2m => sig_axis_cc_s2m(0)
);


u_interconnect_cc : axis_interconnect_cc
  PORT MAP (
    clk_i => clk_i,
    rst_n_i => rst_n_i,
    
    m_axis_cc_m2s => m_axis_cc_m2s,
    m_axis_cc_s2m => m_axis_cc_s2m,
           

    s_axis_cc_m2s => sig_axis_cc_m2s,
    s_axis_cc_s2m => sig_axis_cc_s2m
  );

   
end Behavioral;
