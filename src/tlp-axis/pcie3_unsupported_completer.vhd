-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : pcie3_unsupported_completer.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.pcie_gen3_pkg.all;


entity pcie3_unsupported_completer is
  Port ( clk_i : in STD_LOGIC;
    rst_n_i : in STD_LOGIC;
         
    s_axis_cq_m2s: in t_axis_cq_m2s;
    s_axis_cq_s2m: out t_axis_cq_s2m;
           
    m_axis_cc_m2s: out t_axis_cc_m2s;
    m_axis_cc_s2m: in t_axis_cc_s2m
  );
end pcie3_unsupported_completer;

architecture Behavioral of pcie3_unsupported_completer is
  type   t_state is (STATE_IDLE, STATE_WAIT_FOR_END, STATE_RESPONDING);  
  signal r_state: t_state := STATE_IDLE;
  
  signal s_s_tdata: t_axis_cq_header;
  signal s_s_tuser: t_axis_cq_tuser;
  signal r_m_tdata: t_axis_cc_theader;
  
begin
  s_s_tdata <= pcie_cq_header_to_type(s_axis_cq_m2s.tdata(127 downto 0));
  s_s_tuser <= f_pcie_cq_tuser_to_type(s_axis_cq_m2s.tuser);
  
  r_m_tdata.lower_address <= "0000000";
  r_m_tdata.address_type <= "00"; -- zeros for all other than Atomic 
  r_m_tdata.byte_count <= f_pcie_calc_total_byte_count(s_s_tuser.first_be, s_s_tuser.last_be, s_s_tdata.dword_count);
  r_m_tdata.locked_read_completion <= '0';
  r_m_tdata.dword_count <= (others => '0');
  r_m_tdata.completion_status <= C_PCIE_COMPLETION_STATUS_UR;
  r_m_tdata.poisoned_completion <= '0';
  r_m_tdata.requester_id <= s_s_tdata.requester_id;
  r_m_tdata.tag <= s_s_tdata.tag;
  r_m_tdata.completer_id.bus_id <= (others => '0');
  r_m_tdata.completer_id.device_function <= s_s_tdata.target_function;
  r_m_tdata.completer_id_enable <= '0';
  r_m_tdata.transaction_class <= s_s_tdata.transaction_class;
  r_m_tdata.attr <= s_s_tdata.attr;
  r_m_tdata.force_ecrc <= '0';
 
  
  p1: process(clk_i)
  begin
    if rising_edge(clk_i) then
      if r_state = STATE_IDLE and s_axis_cq_m2s.tvalid = '1' then
        m_axis_cc_m2s.tdata(95 downto 0) <= f_pcie_cc_header_to_val(r_m_tdata);
        m_axis_cc_m2s.tdata(127 downto 96) <= x"00" & s_s_tuser.tph_st_tag & "00000" & s_s_tuser.tph_type & s_s_tuser.tph_present & s_s_tuser.last_be & s_s_tuser.last_be;
        m_axis_cc_m2s.tdata(255 downto 128) <= s_axis_cq_m2s.tdata(127 downto 0); -- copy first data
        m_axis_cc_m2s.tkeep <= "11111111";
        m_axis_cc_m2s.tlast <= '1';
        if s_axis_cq_m2s.tlast = '1' then
          r_state <= STATE_RESPONDING;
          m_axis_cc_m2s.tvalid <= '1';
          s_axis_cq_s2m.tready <= '0';
        else
          r_state <= STATE_WAIT_FOR_END;
          m_axis_cc_m2s.tvalid <= '0';
          s_axis_cq_s2m.tready <= '1';
        end if;
      end if;
      
      if r_state = STATE_WAIT_FOR_END and s_axis_cq_m2s.tvalid = '1' then
        if s_axis_cq_m2s.tlast = '1' then
          r_state <= STATE_RESPONDING;
          m_axis_cc_m2s.tvalid <= '1';
          s_axis_cq_s2m.tready <= '0';
        else
          r_state <= STATE_WAIT_FOR_END;
          m_axis_cc_m2s.tvalid <= '0';
          s_axis_cq_s2m.tready <= '1';
        end if;
      end if;
      
      if r_state = STATE_RESPONDING and m_axis_cc_s2m.tready = '1' then
        r_state <= STATE_IDLE;
        m_axis_cc_m2s.tvalid <= '0';
        s_axis_cq_s2m.tready <= '1';
      end if;
      
      if rst_n_i = '0' then
         r_state <=   STATE_IDLE;
         m_axis_cc_m2s <= c_axis_cc_m2s_zero;    
         m_axis_cc_m2s.tvalid <= '0';
         s_axis_cq_s2m.tready <= '1';
      end if;
    end if;
  end process;


end Behavioral;
