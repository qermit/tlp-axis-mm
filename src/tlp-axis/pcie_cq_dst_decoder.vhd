-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : pcie_cq_dst_decoder.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.pcie_gen3_pkg.all;


entity pcie_cq_dst_decoder is
    generic (
      g_fallback: integer:= -1;
      g_routing: a_pcie_gen3_routing(2 downto 0) := (others => c_pcie_gen3_routing_record_disabled)
    );
    Port ( clk_i : in STD_LOGIC;
           rst_n_i : in STD_LOGIC;
           
           s_axis_cq_m2s: in t_axis_cq_m2s;
           s_axis_cq_s2m: out t_axis_cq_s2m;
           
           m_tdest : out std_logic_vector(1 downto 0);
           m_axis_cq_m2s: out t_axis_cq_m2s;
           m_axis_cq_s2m: in t_axis_cq_s2m
   );
end pcie_cq_dst_decoder;

architecture Behavioral of pcie_cq_dst_decoder is
  type    t_state is (STATE_EMPTY, STATE_HAS_DATA, STATE_FULL);  
  type    t_packet_state is (PKT_STATE_IDLE, PKT_STATE_IN_PROGRESS);

  signal r_state : t_state := STATE_EMPTY;
  signal r_packet_state : t_packet_state := PKT_STATE_IDLE;


  signal r_data : a_axis_cq_m2s(1 downto 0);

-----------  
  signal s_route_port: std_logic_vector(1 downto 0) := (others => '0');  
  signal r_0_route_port: std_logic_vector(1 downto 0);
  signal r_1_route_port: std_logic_vector(1 downto 0);
-----------
 component pcie3_dst_to_port is
  generic (
    g_fallback: integer:= -1;
    g_routing: a_pcie_gen3_routing(2 downto 0) := (others => c_pcie_gen3_routing_record_disabled)
  );
  Port (
    tdata : in STD_LOGIC_VECTOR (255 downto 0);
    oport : out STD_LOGIC_VECTOR (1 downto 0)    
  );
  end component pcie3_dst_to_port;
  

  signal s_s_tdata: t_axis_cq_header;
  signal s_s_tuser: t_axis_cq_tuser;

begin
  s_s_tdata <= pcie_cq_header_to_type(s_axis_cq_m2s.tdata(127 downto 0));
  s_s_tuser <= f_pcie_cq_tuser_to_type(s_axis_cq_m2s.tuser);

 m_axis_cq_m2s.tvalid <= '1' when r_state = STATE_HAS_DATA or r_state = STATE_FULL else '0';
 s_axis_cq_s2m.tready <= '1' when r_state = STATE_EMPTY or r_state = STATE_HAS_DATA else '0'; 
 m_axis_cq_m2s.tdata <= r_data(0).tdata;
 m_axis_cq_m2s.tuser <= r_data(0).tuser;
 m_axis_cq_m2s.tkeep <= r_data(0).tkeep;
 m_axis_cq_m2s.tlast <= r_data(0).tlast;
 
 

 u_dst_to_port: pcie3_dst_to_port
 generic map (
   g_fallback => g_fallback,
   g_routing => g_routing
 )
 port map (
    tdata => s_axis_cq_m2s.tdata,
    oport => s_route_port 
 );
 
 p1: process(clk_i)
 begin
   if rising_edge(clk_i) then
     case r_state is
       when STATE_EMPTY =>
         if s_axis_cq_m2s.tvalid = '1' then
           r_data(0).tdata  <= s_axis_cq_m2s.tdata; 
           r_data(0).tuser  <= s_axis_cq_m2s.tuser;
           r_data(0).tkeep  <= s_axis_cq_m2s.tkeep;
           r_data(0).tlast  <= s_axis_cq_m2s.tlast;
           r_state <= STATE_HAS_DATA;
         end if;
       when STATE_HAS_DATA =>
         if m_axis_cq_s2m.tready = '1' and s_axis_cq_m2s.tvalid = '1' then
           r_data(0).tdata  <= s_axis_cq_m2s.tdata;
           r_data(0).tuser  <= s_axis_cq_m2s.tuser;
           r_data(0).tkeep  <= s_axis_cq_m2s.tkeep;
           r_data(0).tlast  <= s_axis_cq_m2s.tlast;
         elsif m_axis_cq_s2m.tready = '1' and s_axis_cq_m2s.tvalid = '0' then
           r_state <= STATE_EMPTY;
         elsif m_axis_cq_s2m.tready = '0' and s_axis_cq_m2s.tvalid = '1' then
           r_state <= STATE_FULL;
           r_data(1).tdata  <= s_axis_cq_m2s.tdata;
           r_data(1).tuser  <= s_axis_cq_m2s.tuser;
           r_data(1).tkeep  <= s_axis_cq_m2s.tkeep;
           r_data(1).tlast  <= s_axis_cq_m2s.tlast;
         end if;          
       when STATE_FULL =>
         if m_axis_cq_s2m.tready = '1' then
           r_data(0).tdata <= r_data(1).tdata;
           r_data(0).tuser <= r_data(1).tuser;
           -- r_data(0).tdest <= r_data(1).tdest;
           r_data(0).tkeep <= r_data(1).tkeep;
           r_data(0).tlast <= r_data(1).tlast;
           r_state <= STATE_HAS_DATA;
         end if;       
     end case;
      
     if rst_n_i = '0' then
       r_data <= (others => c_axis_cq_m2s_zero);
       r_state <= STATE_EMPTY;
     end if;
   end if;
 end process;
 
 
 p3_packet_state: process(clk_i)
 begin
   if rising_edge(clk_i) then
     if (r_state = STATE_EMPTY or r_state = STATE_HAS_DATA) and s_axis_cq_m2s.tvalid = '1' then
       if r_packet_state = PKT_STATE_IDLE and s_axis_cq_m2s.tlast /= '1' then
         r_packet_state <= PKT_STATE_IN_PROGRESS;
       end if;
       if r_packet_state = PKT_STATE_IN_PROGRESS and s_axis_cq_m2s.tlast = '1' then
         r_packet_state <= PKT_STATE_IDLE;
       end if;
     end if;
     
     if rst_n_i = '0' then
       r_packet_state <= PKT_STATE_IDLE;
     end if;
   end if;
 end process;
 
 
 p2_tdest: process(clk_i)
 begin
   if rising_edge(clk_i) then
     case r_state is
       when STATE_EMPTY =>
         if s_axis_cq_m2s.tvalid = '1' then
           if r_packet_state = PKT_STATE_IDLE then
             r_0_route_port  <= s_route_port;
           end if; 
         end if;
       when STATE_HAS_DATA =>
         if m_axis_cq_s2m.tready = '1' and s_axis_cq_m2s.tvalid = '1' then
           if r_packet_state = PKT_STATE_IDLE then
             r_0_route_port <= s_route_port;
           end if; 
         elsif m_axis_cq_s2m.tready = '0' and s_axis_cq_m2s.tvalid = '1' then
           if r_packet_state = PKT_STATE_IDLE then
             r_1_route_port <= s_route_port;
           end if; 
         end if;          
       when STATE_FULL =>
         if m_axis_cq_s2m.tready = '1' then
           r_0_route_port <= r_1_route_port;
         end if;       
     end case;
      
     if rst_n_i = '0' then
       r_0_route_port <= (others => '0');
       r_1_route_port <= (others => '0');
     end if;
   end if;
 
 end process;
 
 m_tdest <= r_0_route_port;



end Behavioral;
