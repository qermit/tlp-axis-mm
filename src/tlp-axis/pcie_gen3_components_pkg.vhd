-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : pcie_gen3_components_pkg.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;

use work.pcie_gen3_pkg.all;

package pcie_gen3_components_pkg is
  COMPONENT pcie_cq_dst_decoder is
    generic (
      g_fallback: integer:= -1;
      g_routing: a_pcie_gen3_routing(2 downto 0) := (others => c_pcie_gen3_routing_record_disabled)
    );
    Port ( clk_i : in STD_LOGIC;
           rst_n_i : in STD_LOGIC;
           
           s_axis_cq_m2s: in t_axis_cq_m2s;
           s_axis_cq_s2m: out t_axis_cq_s2m;
           
           m_tdest : out std_logic_vector(1 downto 0);
           m_axis_cq_m2s: out t_axis_cq_m2s;
           m_axis_cq_s2m: in t_axis_cq_s2m
   );
   end COMPONENT pcie_cq_dst_decoder;

 component pcie3_dst_to_port is
  generic (
    g_fallback: integer:= -1;
    g_routing: a_pcie_gen3_routing(2 downto 0) := (others => c_pcie_gen3_routing_record_disabled)
  );
  Port (
    tdata : in STD_LOGIC_VECTOR (255 downto 0);
    oport : out STD_LOGIC_VECTOR (1 downto 0)    
  );
  end component pcie3_dst_to_port;

  COMPONENT pcie3_tlp_interconnect is
  generic (
      g_fallback: integer:= -1;
      g_routing: a_pcie_gen3_routing(2 downto 0) := (others => c_pcie_gen3_routing_record_disabled)
  );
  Port ( clk_i : in STD_LOGIC;
         rst_n_i : in STD_LOGIC;
         
         s_axis_cq_m2s: in  t_axis_cq_m2s;
         s_axis_cq_s2m: out t_axis_cq_s2m;
         m_axis_cc_m2s: out t_axis_cc_m2s;
         m_axis_cc_s2m: in  t_axis_cc_s2m;
         
         m_axis_cq_m2s: out a_axis_cq_m2s(2 downto 0);
         m_axis_cq_s2m: in  a_axis_cq_s2m(2 downto 0);
         s_axis_cc_m2s: in  a_axis_cc_m2s(2 downto 0);
         s_axis_cc_s2m: out a_axis_cc_s2m(2 downto 0)
);
end  COMPONENT pcie3_tlp_interconnect;

end package pcie_gen3_components_pkg;

package body pcie_gen3_components_pkg is
   
end package body pcie_gen3_components_pkg;

 
