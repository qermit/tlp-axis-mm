-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : pcie_gen3_pkg.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;


package pcie_gen3_pkg is
  constant C_PCIE_DATA_WIDTH : natural:= 256;
  constant C_PCIE_KEEP_WIDTH : natural:= C_PCIE_DATA_WIDTH / 32;
  constant C_PCIE_USER_WIDTH : natural:= 85;
  constant C_PCIE_REQUEST_TYPE_MEMORY_READ  : std_logic_vector(3 downto 0) := "0000";
  constant C_PCIE_REQUEST_TYPE_MEMORY_WRITE : std_logic_vector(3 downto 0) := "0001";
  constant C_PCIE_REQUEST_TYPE_IO_READ      : std_logic_vector(3 downto 0) := "0010";
  constant C_PCIE_REQUEST_TYPE_IO_WRITE     : std_logic_vector(3 downto 0) := "0011";

  constant C_PCIE_COMPLETION_STATUS_SUCCESS : std_logic_vector(2 downto 0):= "000";
  constant C_PCIE_COMPLETION_STATUS_UR : std_logic_vector(2 downto 0):= "001";
  constant C_PCIE_COMPLETION_STATUS_CA : std_logic_vector(2 downto 0):= "100";


  -- completer reque
  type t_axis_cq_m2s is record
    tdata  : std_logic_vector(C_PCIE_DATA_WIDTH-1 DOWNTO 0);
    tkeep  : std_logic_vector(C_PCIE_KEEP_WIDTH-1 DOWNTO 0);
    tlast  : std_logic;
    tuser  : std_logic_vector(C_PCIE_USER_WIDTH-1 DOWNTO 0);
    tvalid : std_logic;
  end record t_axis_cq_m2s;

  type t_axis_cq_s2m is record
    tready : std_logic;
  end record t_axis_cq_s2m;

  type a_axis_cq_m2s is array (integer range <>) of t_axis_cq_m2s;
  type a_axis_cq_s2m is array (integer range <>) of t_axis_cq_s2m;



  constant c_axis_cq_m2s_zero : t_axis_cq_m2s := (
    tdata  => (others => '0'),
    tkeep  => (others => '0'),
    tlast  => '0',
    tuser  => (others => '0'),
    tvalid => '0');
  
  constant c_axis_cq_s2m_zero : t_axis_cq_s2m := ( tready => '0');

  type t_requester_id is record
    device_function: std_logic_vector (7 downto 0);
    bus_id: std_logic_vector(7 downto 0);
  end record t_requester_id;
  
  constant c_requester_id_zero: t_requester_id := (
     device_function => (others => '0'),
     bus_id => (others => '0')
   );

type t_pcie_request_type is (
 pcie_req_mr,   -- Memory Read Request
 pcie_req_mw,   -- Memory Write Request
 pcie_req_ior,  -- I/O Read Request
 pcie_req_iow,  -- I/O Write Request
 pcie_req_mfaa, -- Memory Fetch and Add Request
 pcie_req_mus,  -- Memory Unconditional Swap Request
 pcie_req_mcas, -- Memory Compare and Swap Request
 pcie_req_lr,   -- Locked Read Request);
 pcie_req_reserved    
 );


  -- Outputs from the FIFO.
  type t_axis_cq_header is record
    address           : std_logic_vector(63 downto 0);
    address_type      : std_logic_vector(1 downto 0);
    attr              : std_logic_vector(2 downto 0);
    transaction_class : std_logic_vector(2 downto 0);
    bar_aperture      : std_logic_vector(5 downto 0);
    bar_id            : std_logic_vector(2 downto 0);
    target_function   : std_logic_vector(7 downto 0);
    tag               : std_logic_vector(7 downto 0);
    requester_id      : t_requester_id;
    req_type          : t_pcie_request_type;
    dword_count       : std_logic_vector(10 downto 0);
  end record t_axis_cq_header;
  
  constant c_axis_cq_header_zero: t_axis_cq_header := (
    address           => (others => '0'),
    address_type      => (others => '0'),
    attr              => (others => '0'),
    transaction_class => (others => '0'),
    bar_aperture      => (others => '0'),
    bar_id            => (others => '0'),
    target_function   => (others => '0'),
    tag               => (others => '0'),
    requester_id      => c_requester_id_zero,
    req_type          => pcie_req_reserved,
    dword_count       => (others => '0')
  );

  type t_axis_cq_tuser is record             -- sideband signal t_axis_cc_m2s.tuser
    first_be: std_logic_vector(3 downto 0);   -- 3:0
    last_be : std_logic_vector(3 downto 0);   -- 7:4
    byte_en: std_logic_vector(31 downto 0);   -- 39:8
    sop: std_logic;                           -- 40
    discontinue: std_logic;                   -- 41
    tph_present: std_logic;                   -- 42
    tph_type: std_logic_vector(1 downto 0);   -- 44:43
    tph_st_tag: std_logic_vector(7 downto 0); -- 52:45
    parity: std_logic_vector(31 downto 0);    -- 84:53
  end record t_axis_cq_tuser;

  constant c_axis_cq_tuser_zero: t_axis_cq_tuser := (
    first_be => (others => '0'),
    last_be => (others => '0'),
    byte_en => (others => '0'),
    sop => '0',
    discontinue => '0',
    tph_present => '0',
    tph_type => (others => '0'),
    tph_st_tag => (others => '0'),
    parity => (others => '0')
  );
  
  -- completer completion signals
  type t_axis_cc_m2s is record
    tdata  : std_logic_vector(C_PCIE_DATA_WIDTH - 1 DOWNTO 0);
    tlast  : std_logic;
    tuser  : std_logic_vector(32 DOWNTO 0);
    tkeep  : std_logic_vector(C_PCIE_KEEP_WIDTH -1  DOWNTO 0);
    tvalid : std_logic;
  end record t_axis_cc_m2s;

  type t_axis_cc_s2m is record
    tready : std_logic;
  end record t_axis_cc_s2m;

  type a_axis_cc_m2s is array (integer range <>) of t_axis_cc_m2s;
  type a_axis_cc_s2m is array (integer range <>) of t_axis_cc_s2m;
    
  constant c_axis_cc_m2s_zero : t_axis_cc_m2s := (
    tdata  => (others => '0'),
    tlast  => '0',
    tuser  => (others => '0'),
    tkeep  => (others => '0'),
    tvalid => '0');
    
    
  constant c_axis_cc_s2m_zero : t_axis_cc_s2m := ( tready => '0');
    


  type t_axis_cc_theader is record
    lower_address: std_logic_vector(6 downto 0); -- 6:0
    address_type : std_logic_vector(1 downto 0); -- 9:8
    byte_count: std_logic_vector(12 downto 0); -- 28:16
    locked_read_completion: std_logic; -- 29
    dword_count: std_logic_vector(10 downto 0); -- 42:32
    completion_status: std_logic_vector(2 downto 0); -- 45:43
    poisoned_completion: std_logic; -- 46
    requester_id: t_requester_id; -- 63:48
    tag: std_logic_vector(7 downto 0); -- 71:64
    completer_id: t_requester_id; -- 87:80
    completer_id_enable: std_logic; -- 88
    transaction_class: std_logic_vector(2 downto 0); -- 91:89
    attr: std_logic_vector(2 downto 0); -- 94:92
    force_ecrc: std_logic; -- 95
  end record t_axis_cc_theader;

  type t_axis_cc_tuser is record
    discontinue: std_logic;
    parity : std_logic_vector(31 downto 0);
  end record t_axis_cc_tuser;


  component pcie_gen3_cr_to_record is
    port (
      tdata : IN std_logic_vector(127 DOWNTO 0);
      cr_request: OUT t_axis_cq_header
    );
  end component pcie_gen3_cr_to_record;

  component pcie_gen3_record_to_cr is
    port (
      cr_request: IN t_axis_cq_header;
      tdata : OUT std_logic_vector(127 DOWNTO 0)
    );
  end component pcie_gen3_record_to_cr;


  type t_pending_packet is record
    attr: std_logic_vector(2 downto 0);
    transaction_class: std_logic_vector(2 downto 0);
    tag: std_logic_vector(7 downto 0);
    requester_id: t_requester_id;
    dword_count: std_logic_vector(10 downto 0);
    total_byte_count: std_logic_vector(12 downto 0);
    address_type: std_logic_vector(1 downto 0);
    address: std_logic_vector(6 downto 0);
  end record t_pending_packet;
 
   function pcie_req_type_to_req_val (req: in t_pcie_request_type) return std_logic_vector;
   function pcie_req_val_to_req_type (req: in std_logic_vector(3 downto 0)) return t_pcie_request_type;
   function pcie_cq_header_to_type(tdata: in std_logic_vector(127 downto 0)) return t_axis_cq_header;
   function pcie_cq_header_to_val(cr_header: in t_axis_cq_header) return std_logic_vector ;
   function f_pcie_cq_tuser_to_type(tuser: in std_logic_vector(84 downto 0)) return t_axis_cq_tuser;
   function f_pcie_cq_tuser_to_val(cq_tuser: in t_axis_cq_tuser) return std_logic_vector;
   
   function f_pcie_cc_header_to_val(cq_tuser: in t_axis_cc_theader) return std_logic_vector;

   function f_pcie_calc_total_byte_count(first_be: std_logic_vector(3 downto 0);
                                         last_be: std_logic_vector(3 downto 0);
                                         dword_count: std_logic_vector(10 downto 0)) return std_logic_vector;

  
   type t_pcie_gen3_routing_record is record
     enabled: boolean;     
     dst_port: natural; -- not used yet
     request_type: t_pcie_request_type; -- not used yet
     routing: std_logic_vector(2 downto 0);
     routing_mask:  std_logic_vector(2 downto 0);
     target_function: std_logic_vector(7 downto 0);
     target_function_mask: std_logic_vector(7 downto 0);
   end record t_pcie_gen3_routing_record;
   
   function f_routing_pcie_gen3_record(
      p_bar_id: natural := -1;
      p_pf_id: natural := -1;
      p_enabled: boolean := true) return t_pcie_gen3_routing_record;
   
    constant c_pcie_gen3_routing_record_disabled: t_pcie_gen3_routing_record := (
     enabled => false,     
     dst_port => 0, -- not used yet
     request_type => pcie_req_reserved, -- not used yet
     routing => (others => '0'),
     routing_mask =>  (others => '0'),
     target_function => (others => '0'),
     target_function_mask => (others => '0')
    );
   

  type a_pcie_gen3_routing is array (natural range <>) of t_pcie_gen3_routing_record;
  function f_pcie_ferst_be_to_lower_address(first_be: std_logic_vector(3 downto 0)) return std_logic_vector;
  function f_pcie_cc_requester_id_to_val(requester_id: in t_requester_id) return  std_logic_vector;

end package pcie_gen3_pkg;

package body pcie_gen3_pkg is
   function pcie_req_val_to_req_type (req: in std_logic_vector(3 downto 0)) return t_pcie_request_type is
       variable v_req_type: t_pcie_request_type;
    begin
        case req is
          when "0000" => v_req_type := pcie_req_mr;
          when "0001" => v_req_type := pcie_req_mw;
          when "0010" => v_req_type := pcie_req_ior;
          when "0011" => v_req_type := pcie_req_iow;
          when others => v_req_type := pcie_req_reserved;
        end case;
        return v_req_type;
    end function;

   function pcie_req_type_to_req_val (req: in t_pcie_request_type) return std_logic_vector  is
       variable v_req_val: std_logic_vector(3 downto 0);
    begin
        case req is
          when pcie_req_mr => v_req_val  := "0000";
          when pcie_req_mw => v_req_val  := "0001";
          when pcie_req_ior => v_req_val := "0010";
          when pcie_req_iow => v_req_val := "0011";
          when others => v_req_val := "1111";
        end case;
        return v_req_val;
    end function;



  
  

  function f_pcie_cc_requester_id_to_type(tdata: in std_logic_vector(15 downto 0)) return  t_requester_id is
      variable v_requester_id: t_requester_id;
  begin
     v_requester_id.device_function := tdata( 7 downto 0);
     v_requester_id.bus_id := tdata(15 downto 8);
     return v_requester_id;
  end function;
  
  function f_pcie_cc_requester_id_to_val(requester_id: in t_requester_id) return  std_logic_vector is
      variable v_tdata: std_logic_vector(15 downto 0);
  begin
     v_tdata(7 downto 0) := requester_id.device_function;
     v_tdata(15 downto 8) := requester_id.bus_id;
     return  v_tdata;
  end function;
  
  

   function pcie_cq_header_to_type(tdata: in std_logic_vector(127 downto 0)) return t_axis_cq_header is
     variable v_cq_request: t_axis_cq_header;
   begin
     v_cq_request.address_type      := tdata(1 downto 0);
     v_cq_request.address           := tdata(63 downto 2) & "00";
     v_cq_request.attr              := tdata(126 downto 124);
     v_cq_request.transaction_class := tdata(123 downto 121);
     v_cq_request.bar_aperture      := tdata(120 downto 115);
     v_cq_request.bar_id            := tdata(114 downto 112);
     v_cq_request.target_function   := tdata(111 downto 104);
     v_cq_request.tag               := tdata(103 downto 96);
     v_cq_request.requester_id      := f_pcie_cc_requester_id_to_type(tdata(95 downto 80));
     v_cq_request.req_type          := pcie_req_val_to_req_type(tdata(78 downto 75));
     v_cq_request.dword_count       := tdata(74 downto 64);
     return v_cq_request;
   end function;
  
   function pcie_cq_header_to_val(cr_header: in t_axis_cq_header) return std_logic_vector is
     variable v_header: std_logic_vector(127 downto 0) := (others => '0');
   begin
     v_header(1 downto 0)     := cr_header.address_type;
     v_header(63 downto 2)    := cr_header.address(63 downto 2);
     v_header(126 downto 124) := cr_header.attr;
     v_header(123 downto 121) := cr_header.transaction_class;
     v_header(120 downto 115) := cr_header.bar_aperture;
     v_header(114 downto 112) := cr_header.bar_id;
     v_header(111 downto 104) := cr_header.target_function;
     v_header(103 downto 96)  := cr_header.tag;
     v_header(95 downto 80)   := f_pcie_cc_requester_id_to_val(cr_header.requester_id);
     v_header(78 downto 75)   := pcie_req_type_to_req_val(cr_header.req_type);
     v_header(74 downto 64)   := cr_header.dword_count;
     return v_header;
   end function;
  
  
  function f_pcie_cq_tuser_to_type(tuser: in std_logic_vector(84 downto 0)) return t_axis_cq_tuser is
    variable v_cq_tuser: t_axis_cq_tuser;
  begin
    v_cq_tuser.first_be    := tuser(3 downto 0);
    v_cq_tuser.last_be     := tuser(7 downto 4);    
    v_cq_tuser.byte_en     := tuser(39 downto 8);
    v_cq_tuser.sop         := tuser(40);
    v_cq_tuser.discontinue := tuser(41);
    v_cq_tuser.tph_present := tuser(42);
    v_cq_tuser.tph_type    := tuser(44 downto 43);
    v_cq_tuser.tph_st_tag  := tuser(52 downto 45);
    v_cq_tuser.parity      := tuser(84 downto 53);
    return v_cq_tuser;
  end function;
  
  function f_pcie_cq_tuser_to_val(cq_tuser: in t_axis_cq_tuser) return std_logic_vector is
    variable v_tuser: std_logic_vector(84 downto 0);
  begin
    v_tuser(3 downto 0)   := cq_tuser.first_be;
    v_tuser(7 downto 4)   := cq_tuser.last_be;    
    v_tuser(39 downto 8)  := cq_tuser.byte_en;
    v_tuser(40)           := cq_tuser.sop;
    v_tuser(41)           := cq_tuser.discontinue;
    v_tuser(42)           := cq_tuser.tph_present;
    v_tuser(44 downto 43) := cq_tuser.tph_type;
    v_tuser(52 downto 45) := cq_tuser.tph_st_tag;
    v_tuser(84 downto 53) := cq_tuser.parity;
    return v_tuser;
  end function;
  
  
  function f_pcie_ferst_be_to_lower_address(first_be: std_logic_vector(3 downto 0)) return std_logic_vector is
    variable v_lower_address: std_logic_vector(1 downto 0) := (others => '0');
  begin
    case first_be is
      when "0000" => v_lower_address(1 downto 0) := "00";
      when "1111"| "1101"| "1011"| "1001"| "0111"| "0101"| "0011"| "0001" => v_lower_address(1 downto 0) := "00";
      when "1110"|"1010"|"0110"|"0010" => v_lower_address(1 downto 0) := "01";
      when "1100"|"0100" => v_lower_address(1 downto 0) := "10";
      when "1000" => v_lower_address(1 downto 0) := "11";
      when others =>  v_lower_address(1 downto 0) := "00";
    end case;
    return v_lower_address;      
  end function;
  
  function f_pcie_cc_header_to_type(tdata: in std_logic_vector(95 downto 0)) return t_axis_cc_theader is
    variable v_cq_tuser: t_axis_cc_theader;
  begin
    v_cq_tuser.lower_address:= tdata(6 downto 0); -- 6:0
    v_cq_tuser.address_type := tdata(9 downto 8); -- 9:8
    v_cq_tuser.byte_count:= tdata(28 downto 16); -- 28:16
    v_cq_tuser.locked_read_completion:= tdata(29); -- 29
    v_cq_tuser.dword_count:= tdata(42 downto 32); -- 42:32
    v_cq_tuser.completion_status:= tdata(2 downto 0); -- 45:43
    v_cq_tuser.poisoned_completion:= tdata(46); -- 46
    v_cq_tuser.requester_id:= f_pcie_cc_requester_id_to_type(tdata(63 downto 48));
    v_cq_tuser.tag:= tdata(71 downto 64);
    v_cq_tuser.completer_id:= f_pcie_cc_requester_id_to_type(tdata(87 downto 72));
    v_cq_tuser.completer_id_enable:= tdata(88);
    v_cq_tuser.transaction_class:= tdata(91 downto 89);
    v_cq_tuser.attr := tdata(94 downto 92);
    v_cq_tuser.force_ecrc:= tdata(95);

    return v_cq_tuser;
  end function;
  
  function f_pcie_cc_header_to_val(cq_tuser: in t_axis_cc_theader) return std_logic_vector is
    variable v_tdata: std_logic_vector(95 downto 0) := (others => '0');
  begin  
    v_tdata(6 downto 0) := cq_tuser.lower_address;
    v_tdata(9 downto 8) := cq_tuser.address_type;
    v_tdata(28 downto 16) := cq_tuser.byte_count;
    v_tdata(29) := cq_tuser.locked_read_completion;
    v_tdata(42 downto 32) := cq_tuser.dword_count;
    v_tdata(2 downto 0) := cq_tuser.completion_status;
    v_tdata(46) := cq_tuser.poisoned_completion;
    v_tdata(63 downto 48) := f_pcie_cc_requester_id_to_val(cq_tuser.requester_id);
    v_tdata(71 downto 64) := cq_tuser.tag;
    v_tdata(87 downto 72) := f_pcie_cc_requester_id_to_val(cq_tuser.completer_id);
    v_tdata(88) := cq_tuser.completer_id_enable;
    v_tdata(91 downto 89) := cq_tuser.transaction_class;
    v_tdata(94 downto 92) := cq_tuser.attr;
    v_tdata(95) := cq_tuser.force_ecrc;
    return v_tdata;
  end function;
  
  
  function f_pcie_calc_total_byte_count(first_be: std_logic_vector(3 downto 0);
                                        last_be: std_logic_vector(3 downto 0);
                                        dword_count: std_logic_vector(10 downto 0)) return std_logic_vector is
    variable v_byte_count: std_logic_vector(12 downto 0):= std_logic_vector(to_unsigned(0, 13));
    constant c_first_last_be: std_logic_vector(7 downto 0):= first_be & last_be;
    constant dword_count_X4: std_logic_vector(12 downto 0) := dword_count  & "00";

  begin
    if last_be = "0000" then
      if first_be(3) = '1' and first_be(0) = '1' then              -- when "1--10000" => v_byte_count := std_logic_vector(to_unsigned(4, 13));
        v_byte_count := std_logic_vector(to_unsigned(4, 13));
      elsif first_be(3 downto 2) = "01" and first_be(0) = '1' then -- when "01-10000" => v_byte_count := std_logic_vector(to_unsigned(3, 13));
        v_byte_count := std_logic_vector(to_unsigned(3, 13));
      elsif first_be(3) = '1' and first_be(1 downto 0) = "10" then --when "1-100000" => v_byte_count := std_logic_vector(to_unsigned(3, 13));
        v_byte_count := std_logic_vector(to_unsigned(3, 13));
      elsif first_be = "0011" or first_be = "0110" or first_be = "1100" then
        v_byte_count := std_logic_vector(to_unsigned(2, 13));
      elsif first_be = "0001"  or first_be = "0010"  or first_be = "0100"  or first_be = "1000"  or first_be = "0000"  then
        v_byte_count := std_logic_vector(to_unsigned(1, 13));
      end if;
    elsif first_be(0) = '1' then
      if  last_be(3) = '1' then
        v_byte_count := std_logic_vector(unsigned(dword_count_X4));
      elsif last_be(3 downto 2) = "01" then
        v_byte_count := std_logic_vector(unsigned(dword_count_X4) - 1);
      elsif last_be(3 downto 1) = "001" then
        v_byte_count := std_logic_vector(unsigned(dword_count_X4) - 2);
      elsif last_be(3 downto 0) = "0001" then
        v_byte_count := std_logic_vector(unsigned(dword_count_X4) - 3);
      end if;
    elsif first_be(1 downto 0) = "10" then
      if  last_be(3) = '1' then
        v_byte_count := std_logic_vector(unsigned(dword_count_X4) - 1);
      elsif last_be(3 downto 2) = "01" then
        v_byte_count := std_logic_vector(unsigned(dword_count_X4) - 2);
      elsif last_be(3 downto 1) = "001" then
        v_byte_count := std_logic_vector(unsigned(dword_count_X4) - 3);
      elsif last_be(3 downto 0) = "0001" then
        v_byte_count := std_logic_vector(unsigned(dword_count_X4) - 4);
      end if;
    elsif first_be(2 downto 0) = "100" then
      if  last_be(3) = '1' then
        v_byte_count := std_logic_vector(unsigned(dword_count_X4) - 2);
      elsif last_be(3 downto 2) = "01" then
        v_byte_count := std_logic_vector(unsigned(dword_count_X4) - 3);
      elsif last_be(3 downto 1) = "001" then
        v_byte_count := std_logic_vector(unsigned(dword_count_X4) - 4);
      elsif last_be(3 downto 0) = "0001" then
        v_byte_count := std_logic_vector(unsigned(dword_count_X4) - 5);
      end if;
    elsif first_be(3 downto 0) = "1000" then
      if  last_be(3) = '1' then
        v_byte_count := std_logic_vector(unsigned(dword_count_X4) - 3);
      elsif last_be(3 downto 2) = "01" then
        v_byte_count := std_logic_vector(unsigned(dword_count_X4) - 4);
      elsif last_be(3 downto 1) = "001" then
        v_byte_count := std_logic_vector(unsigned(dword_count_X4) - 5);
      elsif last_be(3 downto 0) = "0001" then
        v_byte_count := std_logic_vector(unsigned(dword_count_X4) - 6);
      end if;
    else
      v_byte_count := std_logic_vector(to_unsigned(0, 13));
    end if;
    return v_byte_count;  
  end function;
  
  function f_routing_pcie_gen3_record(
    p_bar_id: integer := -1;
    p_pf_id: integer := -1;
    p_enabled:boolean := true
  ) return t_pcie_gen3_routing_record is
    variable v_rr: t_pcie_gen3_routing_record := c_pcie_gen3_routing_record_disabled;    
  begin
     v_rr.enabled := p_enabled;
     case p_bar_id is
       when 0 => 
         v_rr.routing := "000"; -- BAR0
         v_rr.routing_mask := "111";
       when 1 =>
         v_rr.routing := "001"; -- BAR1
         v_rr.routing_mask := "111";
       when 2 =>
         v_rr.routing := "010"; -- BAR2    
         v_rr.routing_mask := "111";
       when 3 =>
         v_rr.routing := "011"; -- BAR3
         v_rr.routing_mask := "111";
       when 4 =>
         v_rr.routing := "100"; -- BAR4
         v_rr.routing_mask := "111";
       when 5 =>
         v_rr.routing      := "101"; -- BAR5
         v_rr.routing_mask := "111";         
       when others => 
         v_rr.routing      := "000";
         v_rr.routing_mask := "000";
     end case;
     case p_pf_id is
       when 0 =>
         v_rr.target_function := "00000000"; -- PF0
         v_rr.target_function_mask := "01000111"; -- BAR5
       when 1 =>
         v_rr.target_function := "00000001"; -- PF1       
         v_rr.target_function_mask := "01000111"; -- BAR5
       when 64 =>
         v_rr.target_function := "01000001"; -- VF0 
         v_rr.target_function_mask := "01000111"; -- BAR5
       when 65 =>
         v_rr.target_function := "01000001"; -- VF1
         v_rr.target_function_mask := "01000111"; -- BAR5
       when 66 =>
         v_rr.target_function := "01000001"; -- VF2
         v_rr.target_function_mask := "01000111"; -- BAR5
       when 67 =>
         v_rr.target_function := "01000001"; -- VF3
         v_rr.target_function_mask := "01000111"; -- BAR5
       when 68 =>
         v_rr.target_function := "01000001"; -- VF4
         v_rr.target_function_mask := "01000111"; -- BAR5
       when 69 =>
         v_rr.target_function := "01000001"; -- VF5
         v_rr.target_function_mask := "01000111"; -- BAR5
       when others =>
         v_rr.target_function := "00000000"; -- ignore
         v_rr.target_function_mask := "00000000"; -- BAR5
     end case;
     
     return v_rr;
  end function;

   
end package body pcie_gen3_pkg;

 
