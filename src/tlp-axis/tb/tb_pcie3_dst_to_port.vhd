-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : tb_pcie3_dst_to_port.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.pcie_gen3_pkg.all;
use work.pcie_gen3_components_pkg.all;


entity tb_pcie3_dst_to_port is
--  Port ( );
end tb_pcie3_dst_to_port;

architecture Behavioral of tb_pcie3_dst_to_port is

constant PERIOD : time := 4ns;

  constant c_pkt1: std_logic_vector(255 downto 0) := x"0000000000000000000000000102030400580000000008010000000000000010"; -- write to port
  constant c_pkt2: std_logic_vector(255 downto 0) := x"0000000000000000000000000000000000580001000000010000000000000010"; -- read from port
  
  constant c_tlp_routing: a_pcie_gen3_routing(2 downto 0) := (
   1 => f_routing_pcie_gen3_record(p_bar_id => 0),
   others => c_pcie_gen3_routing_record_disabled
);
constant c_tlp_fallback: integer:= 0;

signal s_axis_cq_m2s: t_axis_cq_m2s:= c_axis_cq_m2s_zero;
signal s_dest_port: std_logic_vector(1 downto 0);
signal clk_i : std_logic;
signal rst_n_i : std_logic;



begin
 clk_stim: process
begin
  clk_i <= '0';
  wait for PERIOD/2;
  clk_i <= '1';
  wait for PERIOD/2;
end process;

rst_stim: process
begin
  wait for PERIOD*4;
  rst_n_i <= '1';
  wait;
end process;
 
 u_dst_to_port: pcie3_dst_to_port
 generic map (
   g_fallback => c_tlp_fallback,
   g_routing => c_tlp_routing
 )
 port map (
    tdata => s_axis_cq_m2s.tdata,
    oport => s_dest_port 
 );
   
stim: process
begin
  wait for PERIOD*15;
  s_axis_cq_m2s.tdata <= c_pkt1;
  wait for PERIOD;
  s_axis_cq_m2s.tdata <= c_pkt2;
  wait for PERIOD;
  s_axis_cq_m2s.tdata <= (others => '1');
  wait;  
end process;  

end Behavioral;
