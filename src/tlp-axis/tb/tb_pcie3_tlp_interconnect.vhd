-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : tb_pcie3_tlp_interconnect.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-13
-- Last update: 2020-09-13
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2017 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.pcie_gen3_pkg.all;


entity tb_pcie3_tlp_interconnect is
--  Port ( );
end tb_pcie3_tlp_interconnect;

architecture Behavioral of tb_pcie3_tlp_interconnect is

component pcie3_tlp_interconnect is
    generic (
      g_fallback: integer:= -1;
      g_routing: a_pcie_gen3_routing(2 downto 0) := (others => c_pcie_gen3_routing_record_disabled)
    );
    Port ( clk_i : in STD_LOGIC;
           rst_n_i : in STD_LOGIC;
           
           s_axis_cq_m2s: in t_axis_cq_m2s;
           s_axis_cq_s2m: out t_axis_cq_s2m;           
           m_axis_cc_m2s: out t_axis_cc_m2s;
           m_axis_cc_s2m: in t_axis_cc_s2m;
           
           m_axis_cq_m2s: out a_axis_cq_m2s(2 downto 0);
           m_axis_cq_s2m: in a_axis_cq_s2m(2 downto 0);
           s_axis_cc_m2s: in a_axis_cc_m2s(2 downto 0);
           s_axis_cc_s2m: out a_axis_cc_s2m(2 downto 0)
           
           
           );
end component pcie3_tlp_interconnect;

constant c_ports: natural:= 3;
constant PERIOD : time := 4ns;

constant c_tlp_routing: a_pcie_gen3_routing(2 downto 0) := (
   0 => f_routing_pcie_gen3_record(p_pf_id => 0),
   1 => f_routing_pcie_gen3_record(p_bar_id => 0),
   others => c_pcie_gen3_routing_record_disabled
);

signal    clk_i :  STD_LOGIC;
signal    rst_n_i :  STD_LOGIC := '0';

  signal         s_axis_cq_m2s:  t_axis_cq_m2s := c_axis_cq_m2s_zero;
  signal         s_axis_cq_s2m:  t_axis_cq_s2m;           
  signal         m_axis_cc_m2s:  t_axis_cc_m2s;
  signal         m_axis_cc_s2m:  t_axis_cc_s2m := (tready => '1');
           
  signal       m_axis_cq_m2s:  a_axis_cq_m2s(2 downto 0);
  signal       m_axis_cq_s2m:  a_axis_cq_s2m(2 downto 0);           
  signal       s_axis_cc_m2s:  a_axis_cc_m2s(2 downto 0) := (others => c_axis_cc_m2s_zero);
  signal       s_axis_cc_s2m:  a_axis_cc_s2m(2 downto 0);

--  constant c_routing : a_pcie_gen3_routing(2 downto 0) := (
--     0 => c_pcie_gen3_routing_record_disabled,
--     1 => f_routing_pcie_gen3_record(p_pf_id => 1),
--     2 => c_pcie_gen3_routing_record_disabled
--  );


signal r_data_index: natural;
signal r_rx_data_index: natural;

constant c_data_to_send : a_axis_cq_m2s:= (
  0 => (tdata => x"0000000000000000000000000102030400580000000008010000000000000010",
   tlast => '1',
   tkeep => x"1f",
   tuser => '0' & x"ffff5dfc001000000000f",
   tvalid => '1'),
  1 => (tdata => x"0000000000000000000000000000000000580001000000010000000000000010",
   tlast => '1',
   tkeep => x"0f",
   tuser => '1' & x"ffff5dfc001000000000f",
   tvalid => '1')
);


constant c_data_to_recv : a_axis_cc_m2s(0 to 2):= (
  (tdata => x"0000000000000000000000000102030400580000000008010000000000000010",
   tlast => '0',
   tkeep => x"1f",
   tuser => "0" & x"00000000",
   tvalid => '1'),
  (tdata => x"0000000000000000000000000102030400580000000008010000000000000010",
   tlast => '0',
   tkeep => x"1f",
   tuser => "0" & x"00000000",
   tvalid => '1'),
  (tdata => x"0000000000000000000000000000000000580001000000010000000000000010",
   tlast => '1',
   tkeep => x"0f",
   tuser => "0" & x"00000000",
   tvalid => '1')
);

begin

uut: pcie3_tlp_interconnect
    generic map(
      g_routing => c_tlp_routing,
      g_fallback => 0
    )
    Port map( clk_i => clk_i,
           rst_n_i => rst_n_i,
           
           s_axis_cq_m2s => s_axis_cq_m2s,
           s_axis_cq_s2m => s_axis_cq_s2m,
           
           m_axis_cc_m2s => m_axis_cc_m2s,
           m_axis_cc_s2m => m_axis_cc_s2m,
           
           m_axis_cq_m2s => m_axis_cq_m2s,
           m_axis_cq_s2m => m_axis_cq_s2m,
           
           s_axis_cc_m2s => s_axis_cc_m2s,
           s_axis_cc_s2m => s_axis_cc_s2m
);

m_axis_cc_s2m.tready <= '1';

--- slave completer request entities
m_axis_cq_s2m(0).tready <= '1';
m_axis_cq_s2m(1).tready <= '1';
m_axis_cq_s2m(2).tready <= '1';




clk_stim: process
begin
  clk_i <= '0';
  wait for PERIOD/2;
  clk_i <= '1';
  wait for PERIOD/2;
end process;

rst_stim: process
begin
  wait for PERIOD*4;
  rst_n_i <= '1';
  wait;
end process;


p_tx: process(clk_i)
begin
  if rising_edge(clk_i) then
    if (s_axis_cq_m2s.tvalid = '1' and s_axis_cq_s2m.tready ='1') or (s_axis_cq_m2s.tvalid='0')  then
      if r_data_index < c_data_to_send'length then
        s_axis_cq_m2s <= c_data_to_send(r_data_index);
        r_data_index <= r_data_index + 1;
      else
        s_axis_cq_m2s <= c_axis_cq_m2s_zero;
      end if;
    end if;
    
    if rst_n_i = '0' then
      r_data_index <= 0;
      s_axis_cq_m2s <= c_axis_cq_m2s_zero;
    end if;
  end if;
end process;


p_rx: process(clk_i)
begin
  if rising_edge(clk_i) then
    if (s_axis_cc_m2s(0).tvalid = '1' and s_axis_cc_s2m(0).tready ='1') or (s_axis_cc_m2s(0).tvalid='0')  then
      if r_rx_data_index < c_data_to_recv'length then
        s_axis_cc_m2s(0) <= c_data_to_recv(r_rx_data_index);
        r_rx_data_index <= r_rx_data_index + 1;
      else
        s_axis_cc_m2s(0) <= c_axis_cc_m2s_zero;
      end if;
    end if;
    
    if rst_n_i = '0' then
      r_rx_data_index <= 0;
      s_axis_cc_m2s(0) <= c_axis_cc_m2s_zero;
    end if;
  end if;
end process;


end Behavioral;
