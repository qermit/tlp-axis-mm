-------------------------------------------------------------------------------
-- Title      :
-- Project    : TLP-AXIS MM
-------------------------------------------------------------------------------
-- File       : cri_slr_top_timeout.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-10-29
-- Last update: 2020-10-29
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2020 Piotr Miedzik
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library general_cores;
use general_cores.wishbone_pkg.all;

library tlp_axis_mm;
use tlp_axis_mm.pcie3_ultrascale_compoments_pkg.all;
use tlp_axis_mm.pcie_gen3_pkg.all;
use tlp_axis_mm.pcie_gen3_components_pkg.all;
use tlp_axis_mm.pcie3_mm_pkg.all;
use tlp_axis_mm.pcie3_mm_components_pkg.all;

library wishbone_bridge;
use wishbone_bridge.wishbone_bridge_pkg.all;

entity cri_slr_top is
  generic(
    g_pcie_block: string:= "X0Y1";
    g_debug_tlp_cq_cc: boolean := true;
    g_debug_wb: boolean := true
  );
  Port (
    pci_exp_txn : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    pci_exp_txp : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    pci_exp_rxn : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    pci_exp_rxp : IN STD_LOGIC_VECTOR(7 DOWNTO 0);

    sys_clk : IN STD_LOGIC;
    sys_clk_gt : IN STD_LOGIC;
    sys_reset : IN STD_LOGIC;
    
    user_clk : OUT STD_LOGIC;
    user_reset : OUT STD_LOGIC;
    user_lnk_up : OUT STD_LOGIC;
    
    wb_clk: in std_logic;
    wb_rst_n: in std_logic
  );
end cri_slr_top;

architecture Behavioral of cri_slr_top is
constant c_tlp_routing: a_pcie_gen3_routing(2 downto 0) := (
   0 => f_routing_pcie_gen3_record(p_bar_id => 0),
   others => c_pcie_gen3_routing_record_disabled
);

  signal user_clk_slr0 :  STD_LOGIC;
  signal user_reset_slr0 : STD_LOGIC;
  signal user_reset_n_slr0 : STD_LOGIC;
  signal link_up_slr0: STD_LOGIC;
        -- debug ports
  signal  cfg_ltssm_state: std_logic_vector(5 downto 0);

  signal slr0_axis_cq_m2s: t_axis_cq_m2s;
  signal slr0_axis_cq_s2m: t_axis_cq_s2m;
  signal slr0_axis_cc_m2s: t_axis_cc_m2s;
  signal slr0_axis_cc_s2m: t_axis_cc_s2m;
  
  signal m_axis_cq_m2s: a_axis_cq_m2s(2 downto 0);
  signal m_axis_cq_s2m: a_axis_cq_s2m(2 downto 0):= (others => c_axis_cq_s2m_zero);
  signal s_axis_cc_m2s: a_axis_cc_m2s(2 downto 0):= (others => c_axis_cc_m2s_zero);
  signal s_axis_cc_s2m: a_axis_cc_s2m(2 downto 0);
  
  signal s_input_m2s: t_wishbone_slave_in;
  signal s_input_s2m: t_wishbone_slave_out;
  
  constant c_zeropage_address: t_wishbone_address_array(0 downto 0) := ( 0 => x"00000010");
  constant c_zeropage_mask: t_wishbone_address_array(0 downto 0)    := ( 0 => x"FFFFFFF0");
  signal m_zeropage_m2s: t_wishbone_master_out_array(c_zeropage_address'length -1 downto 0);
  signal m_zeropage_s2m: t_wishbone_master_in_array(c_zeropage_address'length -1 downto 0);

  signal m_wb_m2s: t_wishbone_master_out;
  signal m_wb_s2m: t_wishbone_master_in := c_STALL_WB_MASTER_IN;


COMPONENT ila_cq_cc

PORT (
    clk : IN STD_LOGIC;

    probe0 : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    probe1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe3 : IN STD_LOGIC_VECTOR(84 DOWNTO 0);
    probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe6 : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    probe7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe8 : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    probe9 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    probe10 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe11 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe12 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
);
END COMPONENT  ;

COMPONENT ila_wishbone

PORT (
    clk : IN STD_LOGIC;

    probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe3 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe5 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe9 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe10 : IN STD_LOGIC_VECTOR(31 DOWNTO 0)
);
END COMPONENT  ;

begin

user_clk <= user_clk_slr0;
user_reset <= user_reset_slr0;
user_lnk_up <= link_up_slr0;

u_pcie_ep: pcie3_ultrascale_ep
  generic map (
    g_pcie_block =>  g_pcie_block
  )
  Port map (
    pci_exp_txn => pci_exp_txn,
    pci_exp_txp => pci_exp_txp,
    pci_exp_rxn => pci_exp_rxn,
    pci_exp_rxp => pci_exp_rxp,

    sys_clk => sys_clk,
    sys_clk_gt => sys_clk_gt,
    sys_reset => sys_reset,

    user_clk => user_clk_slr0,
    user_reset => user_reset_slr0,
    user_lnk_up => link_up_slr0,
    cfg_ltssm_state => cfg_ltssm_state,

    m_axis_cq_m2s => slr0_axis_cq_m2s,
    m_axis_cq_s2m => slr0_axis_cq_s2m,
    s_axis_cc_m2s => slr0_axis_cc_m2s,
    s_axis_cc_s2m => slr0_axis_cc_s2m
  );

  user_reset_n_slr0 <= not user_reset_slr0;

GEN_ILA: if g_debug_tlp_cq_cc = true generate
  u_ila_cq_cc : ila_cq_cc
  PORT MAP (
    clk => user_clk_slr0,

    probe0 => slr0_axis_cq_m2s.tdata, 
    probe1 => slr0_axis_cq_m2s.tkeep, 
    probe2 => (0 => slr0_axis_cq_m2s.tlast), 
    probe3 => slr0_axis_cq_m2s.tuser, 
    probe4 => (0 => slr0_axis_cq_m2s.tvalid), 
    probe5 => (0 => slr0_axis_cq_s2m.tready), 
    probe6 => slr0_axis_cc_m2s.tdata, 
    probe7 => (0 => slr0_axis_cc_m2s.tlast), 
    probe8 => slr0_axis_cc_m2s.tuser, 
    probe9 => slr0_axis_cc_m2s.tkeep, 
    probe10 => (0 => slr0_axis_cc_m2s.tvalid), 
    probe11 => (0 => slr0_axis_cc_s2m.tready),
    probe12 => (0 => user_reset_n_slr0)
  );
end generate;

u_tlp_interconnect: entity tlp_axis_mm.pcie3_tlp_interconnect
    generic map(
      g_routing => c_tlp_routing,
      g_fallback => -1
    )
    Port map( clk_i => user_clk_slr0,
           rst_n_i => user_reset_n_slr0,
           
           s_axis_cq_m2s => slr0_axis_cq_m2s,
           s_axis_cq_s2m => slr0_axis_cq_s2m,
           
           m_axis_cc_m2s => slr0_axis_cc_m2s,
           m_axis_cc_s2m => slr0_axis_cc_s2m,
           
           m_axis_cq_m2s => m_axis_cq_m2s,
           m_axis_cq_s2m => m_axis_cq_s2m,
           
           s_axis_cc_m2s => s_axis_cc_m2s,
           s_axis_cc_s2m => s_axis_cc_s2m
);

BLK_TLP_TO_WB: block
  signal m_wb_m2s: t_wishbone_a32_d32_m2s;
  signal m_wb_s2m: t_wishbone_a32_d32_s2m;
begin  
	u_tlp_axis_wb:  entity tlp_axis_mm.pcie3_tlp_to_wb
    generic map (
       g_asynchronous => true
    )
    Port  map ( clk_i   => user_clk_slr0,
           rst_n_i  =>  user_reset_n_slr0,

           clk2_i   => wb_clk,
           rst2_n_i => wb_rst_n,

           -- slave completer request interface
           s_axis_cq_m2s => m_axis_cq_m2s(0),
           s_axis_cq_s2m => m_axis_cq_s2m(0),
           -- master completer completion interface
           m_axis_cc_m2s => s_axis_cc_m2s(0),
           m_axis_cc_s2m => s_axis_cc_s2m(0),

           -- Wishbone master interface
           m_wb_m2s => m_wb_m2s,
           m_wb_s2m => m_wb_s2m

           );

  s_input_m2s.dat <= m_wb_m2s.dat;
  s_input_m2s.adr <= m_wb_m2s.adr;
  s_input_m2s.stb <= m_wb_m2s.stb;
  s_input_m2s.cyc <= m_wb_m2s.cyc;
  s_input_m2s.sel <= m_wb_m2s.sel;
  s_input_m2s.we <= m_wb_m2s.we;

  m_wb_s2m.ack   <= s_input_s2m.ack;
  m_wb_s2m.err   <= s_input_s2m.err;
  m_wb_s2m.rty   <= s_input_s2m.rty;
  m_wb_s2m.stall <= s_input_s2m.stall;
  m_wb_s2m.dat   <= s_input_s2m.dat;


  u_ila_wishbone : ila_wishbone
PORT MAP (
  clk => wb_clk,

  probe0 => (0 => m_wb_m2s.cyc),
  probe1 => (0 => m_wb_m2s.stb),
  probe2 => m_wb_m2s.adr,
  probe3 => m_wb_m2s.sel,
  probe4 => (0 => m_wb_m2s.we),
  probe5 => m_wb_m2s.dat,
  probe6 => (0 => m_wb_s2m.ack),
  probe7 => (0 => m_wb_s2m.err),
  probe8 => (0 => m_wb_s2m.rty),
  probe9 => (0 => m_wb_s2m.stall),
  probe10 => m_wb_s2m.dat
);

end block;

u_wb_wb: entity wishbone_bridge.xwb_wb_wb
  generic map (
    -- Address of the slaves connected
    g_zeropage_address     => c_zeropage_address,
    g_zeropage_mask        => c_zeropage_mask,
    -- output interface mode
    g_wishbone_mode        => CLASSIC,
    g_wishbone_granularity => WORD)
  port map (
    clk_sys_i   =>  wb_clk,
    rst_n_i     =>  wb_rst_n,
    -- slave connection from ... , PIPELINED
    s_input_m2s => s_input_m2s,
    s_input_s2m => s_input_s2m,
    -- Master connection to the Wishbone slaves
    m_output_m2s => m_wb_m2s,
    m_output_s2m => m_wb_s2m,
    -- additional zero page, PIPELINED 
    m_zeropage_m2s => m_zeropage_m2s,
    m_zeropage_s2m => m_zeropage_s2m
  );

end Behavioral;
